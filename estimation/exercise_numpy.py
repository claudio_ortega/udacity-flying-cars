import numpy as np

def main():

    # ground truth state
    x0 = np.array([5,6])
    print("x0:{}".format(x0))

    x1 = x0.reshape(1, 2)
    print("x1:{}".format(x1))

    x2 = x0.reshape(2,1)
    print("x2:{}".format(x2))

    x3 = x0.reshape(-1,1)
    print("x3:{}".format(x3))

    x1t = x1.T
    print("x1t:{}".format(x1t))

    # ground truth state
    x = np.array([-0.85, 0.25]).reshape(2, 1)
    print("x:{}".format(x))

    # state -> measurement map
    H = np.array([1, -1]).reshape(1, 2)
    print("H:{}".format(H))

    # noise
    R = np.eye(1) * 0.01
    print("R:{}".format(R))

    R2 = np.eye(2) * 0.01
    print("R2:{}".format(R2))


if __name__ == "__main__":
    main()


