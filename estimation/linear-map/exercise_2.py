import numpy as np


def recursive_linear_estimation(
        R,
        Rinv,
        H,
        x,
        x_hat0,
        Q0,
        n_samples):

    x_hat = np.copy(x_hat0)
    Q = np.copy(Q0)

    for _ in range(n_samples):

        # take next measurement
        y_obs = H @ x + np.random.multivariate_normal([0.], R)

        # update Q and x_hat
        Q = np.linalg.inv( np.linalg.inv(Q) + H.T @ Rinv @ H )
        x_hat = x_hat + Q @ H.T @ Rinv @ (y_obs - H @ x_hat)

    return x_hat, Q


def main():

    # number of samples
    n_samples = 1000
    print("n_samples:{}".format(n_samples))

    # ground truth state
    x = np.array([-0.85, 0.25]).reshape(2, 1)
    print("x:{}".format(x))

    # state -> measurement map
    H = np.array([1, -1]).reshape(1, 2)
    print("H:{}".format(H))

    # noise
    R = np.eye(1) * 0.01
    print("R:{}".format(R))

    x_hat0 = np.array([0, 0]).reshape(-1, 1)  # make it one column
    print("x_hat0:{}".format(x_hat0))

    Q0 = np.eye(len(x_hat0))
    print("Q0:{}".format(Q0))

    x_hat, Q = recursive_linear_estimation(R, np.linalg.inv(R), H, x, x_hat0, Q0, n_samples)
    print("Q:{}".format(Q))

    print("x:{}".format(x.squeeze()))
    print("x_hat:{}".format(x_hat.squeeze()))

    print("H@x:{}".format(H @ x.squeeze()))
    print("H@x_hat:{}".format(H @ x_hat.squeeze()))


if __name__ == "__main__":
    main()


