from CoaxialDrone import CoaxialCopter
from PIDcontroller import PIDController_with_ff
from PathGeneration import flight_path
from DronewithPIDControllerKF import DronewithPIDKF
from DronewithPIDControllerKF import DronewithPIDKFKnobs
from kf import KF
from ipywidgets import interactive


class IMU:
    def __init__(self):
        pass

    def measure(self, z, sigma=0.001):
        return z + np.random.normal(0.0, sigma)


def main():

    total_time = 10.0  # Total flight time
    dt = 0.01  # Time intervale between measurements

    t, z_path, z_dot_path, z_dot_dot_path = flight_path(total_time, dt, 'constant')

    sensor_error = 0.1
    velocity_sigma = 0.1
    position_sigma = 0.1

    MYKF = KF(sensor_error, velocity_sigma, position_sigma, dt)

    FlyingDrone = DronewithPIDKF(z_path, z_dot_path, z_dot_dot_path, t, dt, IMU, KF)
    interactive_plot = interactive(FlyingDrone.PID_controler_with_KF,
                                   position_sigma = (0.0, 0.1, 0.001),
                                   motion_sigma = (0.0, 0.1, 0.001))
    output = interactive_plot.children[-1]
    output.layout.height = '800px'
    interactive_plot

    FlyingDroneKnobs = DronewithPIDKFKnobs(z_path, z_dot_path, z_dot_dot_path, t, dt, IMU, KF)
    interactive_plot = interactive(FlyingDroneKnobs.PID_controler_with_KF_knobs,
                                   k_p=(5.0, 35.0, 1),
                                   k_d=(0.0, 10, 0.5),
                                   k_i=(0.0, 10, 0.5),
                                   mass_err =(0.7, 1.31, 0.01),
                                   sigma = (0.0, 0.1, 0.001),
                                   position_sigma = (0.0, 0.1, 0.001),
                                   motion_sigma = (0.0, 0.1, 0.001))

    output = interactive_plot.children[-1]
    output.layout.height = '800px'
    interactive_plot


if __name__ == "__main__":
    main()


