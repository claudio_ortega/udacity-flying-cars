import numpy as np
#import math
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
from ipywidgets import interactive
from scipy.stats import multivariate_normal
from StateSpaceDisplay import state_space_display, state_space_display_updated
from kf import KF

def main():
    pylab.rcParams['figure.figsize'] = 10, 10

    z = 0.0                         # Initial position
    v = 1.0                         # Initial velocity
    dt = 1.0                        # The time difference between measures
    sensor_error = 0.1              # Sensor sigma
    velocity_sigma = 0.1            # Velocity uncertainty
    position_sigma = 0.1            # Position uncertainty


    mu_0 = np.array([[v],
                     [z]])

    sigma_0 = np.array([[velocity_sigma**2, 0.0],
                        [0.0, position_sigma**2]])

    u = np.array([[0.0],
                  [0.0]])     # no control input is given \ddot{z} = 0


    # Initialize the object
    MYKF = KF(sensor_error, velocity_sigma, position_sigma, dt)

    # Input the initial values
    MYKF.initial_values(mu_0, sigma_0)

    # Call the predict function
    mu_bar, sigma_bar = MYKF.predict(u)

    print('mu_bar = \n', mu_bar)
    print('sigma_bar = \n', sigma_bar)

    state_space_display(z, v, mu_0, sigma_0, mu_bar, sigma_bar)

    measure = 1.01 # only measuring the Z coordinate

    mu_updated, sigma_updated = MYKF.update(measure)

    print('updated mean = \n', mu_updated)
    print('updated sigma = \n', sigma_updated)

    state_space_display_updated(z, v, mu_0, sigma_0, mu_bar, sigma_bar, mu_updated, sigma_updated)


if __name__ == "__main__":
    main()


