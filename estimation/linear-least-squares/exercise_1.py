import numpy as np
import matplotlib.pyplot as plt


def generate_H(m, degree, t):
    """
    Creates a matrix where each row is of the form:
        [t**degree, t**(degree-1), ..., 1]
    """
    # shape:(rows,columns)
    H = np.zeros((m, degree))
    for i in np.arange(degree - 1, -1, -1):
        H[:, -i - 1] = t ** i
    return H


def main():
    # number of samples
    # the larger this value the more accurate the x hat will be.
    n_samples = 100

    # size of the state,
    # the state consists of the coefficients of a polynomial of degree n-1
    n = 4

    # the values for the variable time for which we are getting
    # our state (x), and after that, y_obs through H*x + v
    t = np.random.uniform(-5, 5, n_samples)
    print("t:{}".format(t))

    # H as in y-tilda = H*x + v, being x the state to be estimated
    # out from y-tilda
    H = generate_H(n_samples, n, t)
    print("H:{}".format(H))

    # why do we generate the state out of a random sample?
    x = np.random.randn(n) * 2
    x = [1., 2., 3., 4.]
    print("x:{}".format(x))

    # we finally generate an observation as y_obs = Hx + v
    y_obs = np.dot(H, x) + np.random.normal(0, 1, size=n_samples)
    print(y_obs)

    Ht = np.matrix.transpose(H)
    print("Ht:{}".format(Ht))

    H1 = np.matmul(Ht,H)
    print("H1:{}".format(H1))

    H1Inv = np.linalg.inv(H1)
    print("H1Inv:{}".format(H1Inv))

    H2 = np.matmul(H1Inv,Ht)
    print("H2:{}".format(H2))

    x_hat_simpler_computation = np.linalg.inv(H.T @ H) @ H.T @ y_obs
    print("x_hat_simpler_computation:{}".format(x_hat_simpler_computation))

    x_hat = np.matmul(H2, y_obs)
    print("x_hat:{}".format(x_hat))

    plt.plot(t, y_obs, 'bx')
    plt.title("Noisy Observations")
    plt.show()


if __name__ == "__main__":
    main()


