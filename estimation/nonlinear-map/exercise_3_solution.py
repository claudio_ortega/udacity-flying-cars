import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA


def f_range(x):
    return LA.norm(x)


def f_bearing(x):
    return np.arctan2(x[1], x[0])


def h(x):
    return np.array([f_range(x), f_bearing(x)])


def jacobian_of_h(x):

    t2 = (x[0] ** 2 + x[1] ** 2)
    t1 = t2 ** 0.5

    return np.array([
        [ x[0] / t1, x[1] / t1],
        [-x[0] / t2, x[1] / t2]
    ]).squeeze()


def recursive_estimation(x, R, x_hat0, Q0, n_samples):
    x_hat = np.copy(x_hat0)
    Q = np.copy(Q0)

    for _ in range(n_samples):
        # sample a measurement
        y_obs = h(x) + np.random.multivariate_normal([0, 0], R)

        # compute the jacobian of h(x_hat)
        H = jacobian_of_h(x_hat)

        # update Q and x_hat
        Q = LA.inv(LA.inv(Q) + H.T @ LA.inv(R) @ H)
        x_hat = x_hat + (Q @ H.T @ LA.inv(R) @ (y_obs - h(x_hat))).reshape(2, 1)

    return x_hat, Q


def main():
    n_samples = 1000

    # Covariance matrix
    # added noise for range and bearing functions
    #
    # NOTE: these are set to low variance values
    # to start with, if you increase them you
    # might more samples to get
    # a good estimate.
    R = np.eye(2)
    R[0, 0] = 0.01
    R[1, 1] = np.radians(1)

    # ground truth state
    x = np.array([1.5, 1])

    x_hat0 = np.array([3., 3]).reshape(-1, 1)
    Q0 = np.eye(len(x_hat0))
    print("Q0:{}".format(Q0))

    x_hat0 = np.array([3, 3]).reshape(-1, 1)  # make it one column
    print("x_hat0:{}".format(x_hat0))

    x_hat, Q = recursive_estimation(x, R, x_hat0, Q0, n_samples)

    print("Q:{}".format(Q))
    print("x:{}".format(x))
    print("x_hat:{}".format(x_hat.reshape(1, -1)))
    print("Hx:{}".format(h(x)))
    print("Hx_hat:{}".format(h(x_hat)))

    print("will take about 10 secs, be patient...")

    errors = []
    Ns = np.arange(0, 300, 1)

    for n in Ns:
        x_hat, _ = recursive_estimation(x, R, x_hat0, Q0, n)
        errors.append(LA.norm(x.squeeze() - x_hat.squeeze()))

    plt.plot(Ns, errors)
    plt.xlabel('Number of samples')
    plt.ylabel('Error')

    plt.show()


if __name__ == "__main__":
    main()


