
(0)
after unzipping the attached file, 
open a cmd line and cd into the top dir 

(1) set python environment
    
    source activate fcnd

(2) cd into the dir

    cd udacity-flying-cars/drone-control/final-project

(3) check the commanded trajectories, using auxiliary script
    
    python main_plot_trajectory.py

(4) start the FCND-Simulator, then clicking on the 'BACKYARD FLYER' icon

(5) start visdom

    python -m visdom.server
    
(6) open browser on http://localhost:8097/    

(7) execute final project code

    python flyer_final_project.py


last lines into stdout:

    ...
    disarm transition
    manual transition
    Closing connection ...
    Maximum Horizontal Error:  1.405228392594392
    Maximum Vertical Error:  0.6726882922599415
    Mission Time:  1.0403130000000003
    Mission Success:  True

after that, a window opens containing plots for commanded and actual trajectories, 
in 1D and 3D

end of python project