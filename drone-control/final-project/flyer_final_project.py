# -*- coding: utf-8 -*-
# started off
# https://github.com/udacity/FCND-Controls/blob/master/controls_flyer.py


import sys
import time
from enum import Enum
import numpy as np
from udacidrone.connection import MavlinkConnection
from udacidrone.messaging import MsgID
from util import load_test_trajectory, load_test_trajectory_debug
from proxy_controller import ProxyController, get_one_point_from_trajectory
from plot2 import plot_commanded_and_actual

#
sys.path.append('../../../FCND-Controls')
from unity_drone import UnityDrone


class States(Enum):
    MANUAL = 0
    ARMING = 1
    TAKEOFF = 2
    WAYPOINT = 3
    LANDING = 4
    DISARMING = 5


class FlyerFinalProject(UnityDrone):

    def __init__(
            self,
            connection,
            position_path,
            t_path,
            yaw_path):

        super().__init__(connection)

        self.target_position = np.array([0.0, 0.0, 0.0])
        self.in_mission = True
        self.check_state = {}

        # initial state
        self.flight_state = States.MANUAL

        # register all your callbacks here
        self.register_callback(MsgID.LOCAL_POSITION, self.local_position_callback)
        self.register_callback(MsgID.LOCAL_VELOCITY, self.velocity_callback)
        self.register_callback(MsgID.STATE, self.state_callback)

        # new callbacks to control the drone
        self.register_callback(MsgID.ATTITUDE, self.attitude_callback)
        self.register_callback(MsgID.RAW_GYROSCOPE, self.gyro_callback)

        #
        self.controller = ProxyController()

        (self.position_trajectory, self.time_trajectory, self.yaw_trajectory) = (position_path, t_path, yaw_path)

        self.takeoff_time = 0
        self.thrust_cmd = 0

        self.accumulate_local_position = []
        self.times_for_accumulate_local_position = []

    def lateral_position_controller(self):

        # Sets the local acceleration target from
        # (1) the local position and local velocity read from file
        # (2) the current time referred to the start time
        (self.local_position_target,
         self.local_velocity_target,
         yaw_cmd,
         index) = get_one_point_from_trajectory(
            self.position_trajectory,
            self.yaw_trajectory,
            self.time_trajectory,
            time.time() - self.takeoff_time)

        self.attitude_target = np.array((0.0, 0.0, yaw_cmd))

        #5
        acceleration_cmd = self.controller.lateral_position_control(
            self.thrust_cmd,
            self.local_position_target[0:2],
            self.local_velocity_target[0:2],
            self.local_position[0:2],
            self.local_velocity[0:2])

        self.local_acceleration_target = np.array(
            [acceleration_cmd[0],
             acceleration_cmd[1],
             0.0])

        self.accumulate_local_position.append(self.local_position)
        self.times_for_accumulate_local_position.append(time.time() - self.takeoff_time)

    def attitude_controller(self):

        # Sets the body rate target using the acceleration target and attitude
        #4
        self.thrust_cmd = self.controller.altitude_control(
            -self.local_position_target[2],
            -self.local_velocity_target[2],
            -self.local_position[2],
            -self.local_velocity[2],
            self.attitude,
            0)

        #2
        roll_pitch_rate_cmd = self.controller.roll_pitch_controller(
            self.local_acceleration_target[0:2],
            self.attitude,
            self.thrust_cmd)

        #3
        yawrate_cmd = self.controller.yaw_control(
            self.attitude_target[2],
            self.attitude[2])

        self.body_rate_target = np.array(
            [roll_pitch_rate_cmd[0],
             roll_pitch_rate_cmd[1],
             yawrate_cmd])

    def bodyrate_controller(self):

        # Commands a moment to the vehicle using the body rate target and body rates
        #1
        moment_cmd = self.controller.body_rate_control(
            self.body_rate_target,
            self.gyro_raw)

        self.cmd_moment(moment_cmd[0],
                        moment_cmd[1],
                        moment_cmd[2],
                        self.thrust_cmd)

    def attitude_callback(self):
        if self.flight_state == States.WAYPOINT:
            self.attitude_controller()

    def gyro_callback(self):
        if self.flight_state == States.WAYPOINT:
            self.bodyrate_controller()

    def local_position_callback(self):

        # obtain the next commanded position from the trajectory
        (commanded_position, _, _, index) = get_one_point_from_trajectory(
            self.position_trajectory,
            self.yaw_trajectory,
            self.time_trajectory,
            time.time() - self.takeoff_time)

        if self.flight_state == States.TAKEOFF:
            if -1.0 * self.local_position[2] > 0.95 * self.target_position[2]:
                self.waypoint_transition(index, commanded_position)

        elif self.flight_state == States.WAYPOINT:
            if index < len(self.position_trajectory) - 1:
                self.waypoint_transition(index, commanded_position)
            else:
                if np.linalg.norm(self.local_velocity[0:2]) < 1.0:
                    self.landing_transition()

    # 5
    def velocity_callback(self):
        if self.flight_state == States.LANDING:
            if self.global_position[2] - self.global_home[2] < 0.1:
                if abs(self.local_position[2]) < 0.1:
                    self.disarming_transition()
        if self.flight_state == States.WAYPOINT:
            self.lateral_position_controller()

    def state_callback(self):
        if self.in_mission:
            if self.flight_state == States.MANUAL:
                self.arming_transition()
            elif self.flight_state == States.ARMING:
                if self.armed:
                    self.takeoff_transition()
            elif self.flight_state == States.DISARMING:
                if ~self.armed & ~self.guided:
                    self.manual_transition()

    def arming_transition(self):
        print("arming transition")
        self.take_control()
        self.arm()
        # set the current location to be the home position
        self.set_home_position(self.global_position[0],
                               self.global_position[1],
                               self.global_position[2])

        self.flight_state = States.ARMING

    def takeoff_transition(self):

        print("takeoff transition")

        target_altitude = 3.0
        self.takeoff_time = time.time()
        self.target_position[2] = target_altitude
        self.takeoff(target_altitude)
        self.flight_state = States.TAKEOFF

    def waypoint_transition(self, time_index, commanded_position):

        self.target_position = commanded_position

        self.local_position_target = np.array(
            (self.target_position[0],
             self.target_position[1],
             self.target_position[2]))

        print("\nwaypoint transition - BEGIN")
        print('self.target position{}'.format(self.target_position))
        print('self.local_position_target:{}'.format(self.local_position_target))
        print('self.position_trajectory[self.waypoint_number]:{}'.format(self.position_trajectory[time_index]))
        print('time_index:{}'.format(time_index))
        print("waypoint transition - END")

        self.flight_state = States.WAYPOINT

    def landing_transition(self):
        print("landing transition")
        self.land()
        self.flight_state = States.LANDING

    def disarming_transition(self):
        print("disarm transition")
        self.disarm()
        self.release_control()
        self.flight_state = States.DISARMING

    def manual_transition(self):
        print("manual transition")
        self.stop()
        self.in_mission = False
        self.flight_state = States.MANUAL

    def start(self):
        self.start_log("Logs", "NavLog.txt")
        print("starting connection")
        super().start()
        self.stop_log()


DEBUG_MODE = True


def main():

    if DEBUG_MODE:
        (position_path, t_path, yaw_path) = load_test_trajectory_debug(0.1)
    else:
        (position_path, t_path, yaw_path) = load_test_trajectory(2.0)

    conn = MavlinkConnection('tcp:127.0.0.1:5760', threaded=False, PX4=False)

    instance = FlyerFinalProject(
        conn,
        position_path,
        t_path,
        yaw_path)

    instance.start()
    instance.print_mission_score()

    plot_commanded_and_actual(
        instance.time_trajectory,
        np.array(instance.position_trajectory)[:, 0],
        np.array(instance.position_trajectory)[:, 1],
        np.array(instance.position_trajectory)[:, 2],

        instance.times_for_accumulate_local_position,
        np.array(instance.accumulate_local_position)[:, 0],
        np.array(instance.accumulate_local_position)[:, 1],
        np.array(instance.accumulate_local_position)[:, 2])


if __name__ == "__main__":
    main()

