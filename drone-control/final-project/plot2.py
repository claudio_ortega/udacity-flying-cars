# -*- coding: utf-8 -*-
"""
Starter code for the controls project.
This is the solution of the backyard flyer script,
modified for all the changes required to get it working for controls.
"""

import sys
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as Axes3D

sys.path.append('../../../FCND-Controls')
sys.path.append('../3D/minimum_snap')


def set_limits(axis, np_array_x, np_array_y, np_array_z):
    axis.set_xlim(np.amin(np_array_x)-5, np.amax(np_array_x)+5)
    axis.set_ylim(np.amin(np_array_y)-5, np.amax(np_array_y)+5)
    axis.set_zlim(np.amax(np_array_z)+5, np.amin(np_array_z)-5)


def set_labels(axis, xlabel, ylabel, zlabel):
    axis.set_xlabel(xlabel)
    axis.set_ylabel(ylabel)
    axis.set_zlabel(zlabel)


def plot_commanded(
        t_path,
        x_path,
        y_path,
        z_path,
        psi_path):

    #
    fig = plt.figure(figsize=(20, 10))

    # 1
    ax1 = fig.add_subplot(331, projection='3d')

    set_limits(ax1, x_path, y_path, z_path)
    set_labels(ax1, "x", "y", "z")

    ax1.plot(
        x_path,
        y_path,
        z_path,
        lw=3.0, alpha=.5, color='red')

    plt.legend(
        ['target position'],
        fontsize=10)

    # 4
    ax4 = fig.add_subplot(334)

    ax4.plot(t_path, x_path)
    ax4.set_xlabel("time")
    ax4.set_ylabel("x")

    plt.legend(
        ['target x'],
        fontsize=10)

    # 5
    ax5 = fig.add_subplot(335)

    ax5.plot(t_path, y_path)
    ax5.set_xlabel("time")
    ax5.set_ylabel("y")

    plt.legend(
        ['target y'],
        fontsize=10)

    # 6
    ax6 = fig.add_subplot(336)

    ax6.plot(t_path, z_path)
    ax6.set_xlabel("time")
    ax6.set_ylabel("z")

    plt.legend(
        ['target z'],
        fontsize=10)

    # 9
    ax9 = fig.add_subplot(339)

    ax9.plot(t_path, psi_path, color='blue')

    ax9.set_xlabel("t")
    ax9.set_ylabel("yaw angle - psi")

    plt.legend(
        ['commanded'],
        fontsize=10)

    #
    plt.show()


def plot_commanded_and_actual(
        t_path_commanded,

        x_path_commanded,
        y_path_commanded,
        z_path_commanded,

        t_path_actual,
        x_path_actual,
        y_path_actual,
        z_path_actual):

    #
    fig = plt.figure(figsize=(20, 10))

    # 1
    ax1 = fig.add_subplot(331, projection='3d')

    set_limits(ax1, x_path_commanded, y_path_commanded, z_path_commanded)
    set_labels(ax1, "x", "y", "z")

    ax1.plot(
        x_path_commanded,
        y_path_commanded,
        z_path_commanded,
        lw=3.0, alpha=.5, color='red')

    ax1.plot(
        x_path_actual,
        y_path_actual,
        z_path_actual,
        lw=1.0, alpha=1.0, color='blue')

    plt.legend(
        ['target position'],
        fontsize=10)

    # 4
    ax4 = fig.add_subplot(334)

    ax4.plot(t_path_commanded, x_path_commanded, lw=1.0, alpha=1.0, color='blue')
    ax4.plot(t_path_actual, x_path_actual, lw=1.0, alpha=1.0, color='red')
    ax4.set_xlabel("time")
    ax4.set_ylabel("x")

    plt.legend(
        ['target x', 'actual x'],
        fontsize=10)

    # 5
    ax5 = fig.add_subplot(335)

    ax5.plot(t_path_commanded, y_path_commanded, lw=1.0, alpha=1.0, color='blue')
    ax5.plot(t_path_actual, y_path_actual, lw=1.0, alpha=1.0, color='red')
    ax5.set_xlabel("time")
    ax5.set_ylabel("y")

    plt.legend(
        ['target y', 'actual y'],
        fontsize=10)

    # 6
    ax6 = fig.add_subplot(336)

    ax6.set_ylim(2.5, -12.5)
    ax6.plot(t_path_commanded, z_path_commanded, lw=1.0, alpha=1.0, color='blue')
    ax6.plot(t_path_actual, z_path_actual, lw=1.0, alpha=1.0, color='red')
    ax6.set_xlabel("time")
    ax6.set_ylabel("z")

    plt.legend(
        ['target z', 'actual z'],
        fontsize=10)

    #
    plt.show()


