import numpy as np


def load_test_trajectory(time_factor):

    """Loads the test_trajectory.txt
    Args:
        time_factor: a multiplier to decrease the total time of the trajectory
    """

    data = np.loadtxt('test_trajectory.txt', delimiter=',', dtype='Float64')
    position_trajectory = []
    time_trajectory = []
    yaw_trajectory = []

    trajectory_length = len(data[:, 0])

    for i in range(0, trajectory_length):
        position_trajectory.append(data[i, 1:4])
        time_trajectory.append(data[i, 0] * time_factor)

    for i in range(0, trajectory_length - 1):
        yaw_trajectory.append(
            np.arctan2(
                position_trajectory[i + 1][1] - position_trajectory[i][1],
                position_trajectory[i + 1][0] - position_trajectory[i][0]))

    yaw_trajectory.append(yaw_trajectory[-1])

    return position_trajectory, time_trajectory, yaw_trajectory


def load_test_trajectory_debug(time_factor):

    """Loads the test_trajectory.txt but it overrides the position with [0,0,-3]
    Args:
        time_factor: a multiplier to decrease the total time of the trajectory
    """

    position_trajectory = []
    time_trajectory = []
    yaw_trajectory = []

    for i in range(1, 50):
        time_trajectory.append(i * 0.1 * time_factor)
        yaw_trajectory.append(0.0)
        if i < 25:
            position_trajectory.append(np.array([0, 0, -3]))
        else:
            position_trajectory.append(np.array([10, 10, -10]))

    return position_trajectory, time_trajectory, yaw_trajectory


def test():
    load_test_trajectory_debug(1.0)
    load_test_trajectory(1.0)


# if __name__ == "__main__":
#     test()


