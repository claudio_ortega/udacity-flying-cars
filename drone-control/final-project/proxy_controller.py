"""
PID Controller

components:
    follow attitude commands
    gps commands and yaw
    waypoint following
"""
import numpy as np
import sys

#
sys.path.append('../3D/exercise5')
from controller import Exercise5Controller
from math import sin, cos


DRONE_MASS_KG = 0.5
GRAVITY = -9.81
MOI = np.array([0.005, 0.005, 0.01])
MAX_THRUST = 10.0
MAX_TORQUE = 1.0

""" 
    This class delegates implementations of the 5 controllers
    into the already coded and tested exercise 5 class

    the only thing it does it keep happy the class 
"""


class ProxyController(object):

    def __init__(self):

        k = 50
        
        """Initialize the controller object and control gains"""
        self.exercise5_controller = Exercise5Controller(
            z_k_p=k*10.0,
            z_k_d=k*14.0,
            x_k_p=k*14.0,
            x_k_d=k*14.0,
            y_k_p=k*14.0,
            y_k_d=k*14.0,
            k_p_roll=k*15.0,
            k_p_pitch=k*15.0,
            k_p_yaw=k*10.0,
            k_p_p=k*40.0,
            k_p_q=k*40.0,
            k_p_r=k*40.0
        )

    # 1
    def body_rate_control(
            self,
            body_rate_cmd,
            body_rate):

        """ Generate the roll, pitch, yaw moment commands in the body frame

        Args:
            body_rate_cmd: 3-element numpy array (p_cmd,q_cmd,r_cmd) in radians/second^2
            body_rate: 3-element numpy array (p,q,r) in radians/second^2

        Returns: 3-element numpy array, desired roll moment, pitch moment, and yaw moment commands in Newtons*meters
        """

        return self.exercise5_controller.body_rate_controller(
            body_rate_cmd[0],
            body_rate_cmd[1],
            body_rate_cmd[2],
            body_rate[0],
            body_rate[1],
            body_rate[2],
        )

    # 2
    def roll_pitch_controller(
            self,
            acceleration_cmd,
            attitude,
            c):

        """ Generate the rollrate and pitchrate commands in the body frame

        Args:
            acceleration_cmd: 2-element numpy array (north_acceleration_cmd,east_acceleration_cmd) in m/s^2
            attitude: 3-element numpy array (roll, pitch, yaw) in radians
            c: vehicle thrust command in Newton

        Returns: 2-element numpy array, desired rollrate (p) and pitchrate (q) commands in radians/s
        """

        return self.exercise5_controller.roll_pitch_controller(
            acceleration_cmd[0] / c,
            acceleration_cmd[1] / c,
            euler_to_rotation_matrix(
                attitude[0],
                attitude[1],
                attitude[2])
        )

    # 3
    def yaw_control(
            self,
            yaw_cmd,
            yaw):
        """ Generates the target yaw rate

        Args:
            yaw_cmd: desired vehicle yaw in radians
            yaw: vehicle yaw in radians

        Returns: target yawrate in radians/sec
        """
        return self.exercise5_controller.yaw_controller(
            yaw_cmd,
            yaw)

    # 4
    def altitude_control(
            self,
            altitude_cmd,
            vertical_velocity_cmd,
            altitude,
            vertical_velocity,
            attitude,
            acceleration_ff=0.0):
        """Generates vertical acceleration (thrust) command

        Args:
            altitude_cmd: desired vertical position (+up)
            vertical_velocity_cmd: desired vertical velocity (+up)
            altitude: vehicle vertical position (+up)
            vertical_velocity: vehicle vertical velocity (+up)
            attitude: the vehicle's current attitude, 3 element numpy array (roll, pitch, yaw) in radians
            acceleration_ff: feed-forward acceleration command (+up)

        Returns: thrust command for the vehicle (+up)
        """
        return self.exercise5_controller.altitude_control(
            altitude_cmd,
            vertical_velocity_cmd,
            acceleration_ff,
            altitude,
            vertical_velocity,
            euler_to_rotation_matrix(
                attitude[0],
                attitude[1],
                attitude[2])
        )

    # 5
    def lateral_position_control(
            self,
            thrust_cmd,
            local_position_cmd,
            local_velocity_cmd,
            local_position,
            local_velocity,
            acceleration_ff=np.array([0.0, 0.0])):
        """Generate horizontal acceleration commands for the vehicle in the local frame

        Args:
            local_position_cmd: desired 2D position in local frame [north, east]
            local_velocity_cmd: desired 2D velocity in local frame [north_velocity, east_velocity]
            local_position: vehicle position in the local frame [north, east]
            local_velocity: vehicle velocity in the local frame [north_velocity, east_velocity]
            acceleration_ff: feedforward acceleration command
            thrust_cmd: total thrust c

        Returns: desired vehicle 2D acceleration in the local frame [north, east]
        """
        return self.exercise5_controller.lateral_controller(
            local_position_cmd[0],
            local_velocity_cmd[0],
            acceleration_ff[0],
            local_position[0],
            local_velocity[0],

            local_position_cmd[1],
            local_velocity_cmd[1],
            acceleration_ff[1],
            local_position[1],
            local_velocity[1],

            thrust_cmd
        )


def get_one_point_from_trajectory(
        position_trajectory,
        yaw_trajectory,
        time_trajectory,
        sample_time):

    """Generate a commanded position, velocity and yaw based on the trajectory

    Args:
        position_trajectory: list of 3-element numpy arrays, NED positions
        yaw_trajectory: list yaw commands in radians
        time_trajectory: list of times (in seconds) that correspond to the position and yaw commands
        sample_time: float corresponding to the current time in seconds

    Returns: tuple (commanded position (x,y,z), commanded velocity (x,y,z), commanded yaw)

    """

    # index_ct is the index for the current time
    index_for_current_time = np.argmin(np.abs(np.array(time_trajectory) - sample_time))
    time_ref = time_trajectory[index_for_current_time]

    if sample_time < time_ref:
        yaw = yaw_trajectory[index_for_current_time - 1]
        position0 = position_trajectory[index_for_current_time - 1]
        position1 = position_trajectory[index_for_current_time]
        time0 = time_trajectory[index_for_current_time - 1]
        time1 = time_trajectory[index_for_current_time]

    else:
        yaw = yaw_trajectory[index_for_current_time]
        if index_for_current_time >= len(position_trajectory) - 1:
            position0 = position_trajectory[index_for_current_time]
            position1 = position_trajectory[index_for_current_time]
            time0 = 0.0
            time1 = 1.0
        else:
            position0 = position_trajectory[index_for_current_time]
            position1 = position_trajectory[index_for_current_time + 1]
            time0 = time_trajectory[index_for_current_time]
            time1 = time_trajectory[index_for_current_time + 1]

    delta_t = time1 - time0
    delta_position = position1 - position0

    commanded_velocity = delta_position / delta_t
    commanded_position = position0 + commanded_velocity * delta_t
    
    return commanded_position, commanded_velocity, yaw, index_for_current_time


def euler_to_rotation_matrix(phi, theta, psi):
    r_x = np.array([[1, 0, 0],
                    [0, cos(phi), -sin(phi)],
                    [0, sin(phi), cos(phi)]])

    r_y = np.array([[cos(theta), 0, sin(theta)],
                    [0, 1, 0],
                    [-sin(theta), 0, cos(theta)]])

    r_z = np.array([[cos(psi), -sin(psi), 0],
                    [sin(psi), cos(psi), 0],
                    [0, 0, 1]])

    return np.matmul(r_z, np.matmul(r_y, r_x))

