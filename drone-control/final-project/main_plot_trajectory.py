# -*- coding: utf-8 -*-
"""
Starter code for the controls project.
This is the solution of the backyard flyer script,
modified for all the changes required to get it working for controls.
"""

import sys
import numpy as np

#
sys.path.append('../../../FCND-Controls')
sys.path.append('../3D/minimum_snap')
from util import load_test_trajectory
from trajectory_3d import compute_sinusoidal_trajectory
from plot2 import plot_commanded


def test1():
    (position_path, t_path, yaw_path) = load_test_trajectory(2.0)
    x_path = np.array(position_path)[:, 0]
    y_path = np.array(position_path)[:, 1]
    z_path = np.array(position_path)[:, 2]

    plot_commanded(
        t_path,
        x_path,
        y_path,
        z_path,
        yaw_path)


def test2():
    (t_path, x_path, y_path, z_path) = compute_sinusoidal_trajectory(1000, 10)
    yaw_path = np.zeros(t_path.size)

    plot_commanded(
        t_path,
        x_path,
        y_path,
        z_path,
        yaw_path)


if __name__ == "__main__":

    test1()
    test2()

