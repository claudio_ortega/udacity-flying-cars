import numpy as np


class Exercise5Controller:

    def __init__(
            self,
            z_k_p,
            z_k_d,
            x_k_p,
            x_k_d,
            y_k_p,
            y_k_d,
            k_p_roll,
            k_p_pitch,
            k_p_yaw,
            k_p_p,
            k_p_q,
            k_p_r):

        self.z_k_p = z_k_p
        self.z_k_d = z_k_d
        self.x_k_p = x_k_p
        self.x_k_d = x_k_d
        self.y_k_p = y_k_p
        self.y_k_d = y_k_d
        self.k_p_roll = k_p_roll
        self.k_p_pitch = k_p_pitch
        self.k_p_yaw = k_p_yaw
        self.k_p_p = k_p_p
        self.k_p_q = k_p_q
        self.k_p_r = k_p_r

        self.g = 9.81

    # 1
    def body_rate_controller(
            self,
            p_c,
            q_c,
            r_c,
            p_actual,
            q_actual,
            r_actual):
        p_err = p_c - p_actual
        u_bar_p = self.k_p_p * p_err

        q_err = q_c - q_actual
        u_bar_q = self.k_p_q * q_err

        r_err = r_c - r_actual
        u_bar_r = self.k_p_r * r_err

        return u_bar_p, u_bar_q, u_bar_r

    # 2
    def roll_pitch_controller(
            self,
            b_x_c,
            b_y_c,
            rot_mat):

        b_x = rot_mat[0, 2]
        b_x_err = b_x_c - b_x
        b_x_p_term = self.k_p_roll * b_x_err

        b_y = rot_mat[1, 2]
        b_y_err = b_y_c - b_y
        b_y_p_term = self.k_p_pitch * b_y_err

        b_z = rot_mat[2, 2]

        rot_mat1 = np.array([[rot_mat[1, 0], -rot_mat[0, 0]], [rot_mat[1, 1], -rot_mat[0, 1]]]) / b_z

        b_x_commanded_dot = b_x_p_term
        b_y_commanded_dot = b_y_p_term

        rot_rate = np.matmul(rot_mat1, np.array([b_x_commanded_dot, b_y_commanded_dot]).T)

        p_c = rot_rate[0]
        q_c = rot_rate[1]

        return p_c, q_c

    # 3
    def yaw_controller(
            self,
            psi_target,
            psi_actual):

        psi_err = psi_target - psi_actual
        r_c = self.k_p_yaw * psi_err

        return r_c

    # 1,2,3
    def attitude_controller(
            self,
            b_x_c_target,
            b_y_c_target,
            psi_target,
            psi_actual,
            p_actual,
            q_actual,
            r_actual,
            rot_mat):

        p_c, q_c = self.roll_pitch_controller(
            b_x_c_target,
            b_y_c_target,
            rot_mat)

        r_c = self.yaw_controller(
            psi_target,
            psi_actual)

        u_bar_p, u_bar_q, u_bar_r = self.body_rate_controller(
            p_c,
            q_c,
            r_c,
            p_actual,
            q_actual,
            r_actual)

        return u_bar_p, u_bar_q, u_bar_r

    # 4
    def altitude_controller(
            self,
            z_target,
            z_dot_target,
            z_dot_dot_target,
            z_actual,
            z_dot_actual,
            rot_mat):

        z_err = z_target - z_actual
        z_err_dot = z_dot_target - z_dot_actual

        p_term = self.z_k_p * z_err
        d_term = self.z_k_d * z_err_dot

        u_1_bar = p_term + d_term + z_dot_dot_target
        b_z = rot_mat[2, 2]
        c = (u_1_bar - self.g) / b_z

        return clip(c)

    # 5
    def lateral_controller(
            self,
            x_target,
            x_dot_target,
            x_dot_dot_target,
            x_actual,
            x_dot_actual,
            y_target,
            y_dot_target,
            y_dot_dot_target,
            y_actual,
            y_dot_actual,
            c):

        x_err = x_target - x_actual
        x_err_dot = x_dot_target - x_dot_actual

        p_term_x = self.x_k_p * x_err
        d_term_x = self.x_k_d * x_err_dot

        x_dot_dot_command = p_term_x + d_term_x + x_dot_dot_target

        c = clip(c)

        b_x_c = x_dot_dot_command / c

        y_err = y_target - y_actual
        y_err_dot = y_dot_target - y_dot_actual

        p_term_y = self.y_k_p * y_err
        d_term_y = self.y_k_d * y_err_dot

        y_dot_dot_command = p_term_y + d_term_y + y_dot_dot_target

        b_y_c = y_dot_dot_command / c

        return b_x_c, b_y_c


def clip(x):
    if x > 0:
        return np.clip(x, 1e-20, None)
    else:
        return np.clip(x, None, -1e-20)
