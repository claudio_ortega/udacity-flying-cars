import sys
import numpy as np
from controller import Exercise5Controller
from model import Exercise5DroneModel
from plotter import plot_all
from enum import Enum

#
sys.path.append('../minimum_snap')
from trajectory_3d import compute_sinusoidal_trajectory, compute_minimum_snap_trajectory, compute_2_derivatives_3D


def run_model_and_controller(
    dt,
    x_path, y_path, z_path,
    x_dot_path, y_dot_path, z_dot_path,
    x_dot_dot_path, y_dot_dot_path, z_dot_dot_path,
    psi_path):

    drone = Exercise5DroneModel()

    controller = Exercise5Controller(
        z_k_p=2.0,
        z_k_d=1.0,
        x_k_p=6.0,
        x_k_d=4.0,
        y_k_p=6.0,
        y_k_d=4.0,
        k_p_roll=8.0,
        k_p_pitch=8.0,
        k_p_yaw=8.0,
        k_p_p=20.0,
        k_p_q=20.0,
        k_p_r=20.0
    )

    # declaring the initial state of the drone with zero
    # height and zero velocity
    drone.set_state(
        np.array(
            [
                x_path[0],
                y_path[0],
                z_path[0],
                0.0,
                0.0,
                psi_path[0],
                x_dot_path[0],
                y_dot_path[0],
                z_dot_path[0],
                0.0,
                0.0,
                0.0
            ]
        )
    )

    # arrays for recording the state history,
    # propeller angular velocities and linear accelerations
    drone_state_history = drone.get_state()
    omega_history = drone.omega
    accelerations = drone.get_linear_acceleration()
    accelerations_history = accelerations
    angular_vel_history = drone.get_euler_derivatives()

    inner_loop_relative_to_outer_loop = 10
    inner_dt = dt / inner_loop_relative_to_outer_loop

    # executing the flight
    for i in range(0, z_path.size):

        rot_mat = drone.get_rotation_matrix()

        c = controller.altitude_controller(
            z_path[i],
            z_dot_path[i],
            z_dot_dot_path[i],
            drone.z,
            drone.z_dot,
            rot_mat)

        b_x_c, b_y_c = controller.lateral_controller(
            x_path[i],
            x_dot_path[i],
            x_dot_dot_path[i],
            drone.x,
            drone.x_dot,
            y_path[i],
            y_dot_path[i],
            y_dot_dot_path[i],
            drone.y,
            drone.y_dot,
            c)

        for j in range(inner_loop_relative_to_outer_loop):

            rot_mat = drone.get_rotation_matrix()

            u_bar_p, u_bar_q, u_bar_r = controller.attitude_controller(
                b_x_c,
                b_y_c,
                psi_path[i],
                drone.psi,
                drone.p,
                drone.q,
                drone.r,
                rot_mat)

            if np.isnan(c) or np.isnan(u_bar_p) or np.isnan(u_bar_q) or np.isnan(u_bar_r):
                print("ERROR - i:{}, j:{}".format(i, j))
                print("ERROR - c:{}, u_bar_p:{}, u_bar_p:{}, u_bar_p:{}".format(c, u_bar_p, u_bar_q, u_bar_r))
                sys.exit()

            drone.set_propeller_angular_velocities(i, j, c, u_bar_p, u_bar_q, u_bar_r)
            drone.advance_state(inner_dt)

        # generating a history of the state history, propeller angular velocities and linear accelerations
        drone_state_history = np.vstack((drone_state_history, drone.get_state()))
        omega_history = np.vstack((omega_history, drone.omega))
        accelerations = drone.get_linear_acceleration()
        accelerations_history = np.vstack((accelerations_history, accelerations))
        angular_vel_history = np.vstack((angular_vel_history, drone.get_euler_derivatives()))

    return drone_state_history, omega_history


class EnumTrajectoryType(Enum):
    MinimumSnap, Lissajous = 'MinimumSnap', 'Lissajous'


def generate_trajectory(trajectory_type):

    if trajectory_type == EnumTrajectoryType.MinimumSnap:
        return compute_minimum_snap_trajectory(
            np.array([0, 4, 8, 12, 16, 20]),
            np.array(
                [[0, 0, 0],
                 [1, 0, 1],
                 [2, 0, 1],
                 [2, 2, 1],
                 [1.5, 1.5, 1.5],
                 [1.5, 1.5, 2.0]]).T,
            400)

    elif trajectory_type == EnumTrajectoryType.Lissajous:
        return compute_sinusoidal_trajectory(1000)

    else:
        print("ERROR - trajectory_type:{}".format(trajectory_type))
        sys.exit(-1)


def main():

    # you may change for EnumTrajectoryType.Lissajous
    (t_path, x_path, y_path, z_path) = generate_trajectory(EnumTrajectoryType.MinimumSnap)

    (x_dot_path, y_dot_path,
     z_dot_path, x_dot_dot_path,
     y_dot_dot_path, z_dot_dot_path) = compute_2_derivatives_3D(t_path, x_path, y_path, z_path)

    psi_path = np.zeros(x_path.size)

    dt = t_path[1] - t_path[0]

    (drone_state_history, omega_history) = run_model_and_controller(
        dt,
        x_path, y_path, z_path,
        x_dot_path, y_dot_path, z_dot_path,
        x_dot_dot_path, y_dot_dot_path, z_dot_dot_path,
        psi_path)

    plot_all(
        t_path,
        x_path,
        y_path,
        z_path,
        psi_path,
        drone_state_history[:-1],
        omega_history[:-1]
    )


if __name__ == "__main__":
    main()

