import sys
import numpy as np
from math import sin, cos, tan, sqrt


class Exercise5DroneModel:

    def __init__(self,
                 k_f=1.0,
                 k_m=1.0,
                 m=0.5,
                 L=0.566,  # full rotor to rotor distance
                 i_x=0.1,
                 i_y=0.1,
                 i_z=0.2):
        self.k_f = k_f
        self.k_m = k_m
        self.m = m
        self.l = L / (2 * sqrt(2))  # perpendicular distance to axes
        self.i_x = i_x
        self.i_y = i_y
        self.i_z = i_z

        # x, y, y, phi, theta, psi, x_dot, y_dot, z_dot, p, q, r
        self.X = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.omega = np.array([0.0, 0.0, 0.0, 0.0])

        self.g = 9.81

    # position
    @property
    def x(self):
        return self.X[0]

    @property
    def y(self):
        return self.X[1]

    @property
    def z(self):
        return self.X[2]

    # euler angles [rad] (in world / lab frame)
    @property
    def phi(self):
        return self.X[3]

    @property
    def theta(self):
        return self.X[4]

    @property
    def psi(self):
        return self.X[5]

    # velocities
    @property
    def x_dot(self):
        return self.X[6]

    @property
    def y_dot(self):
        return self.X[7]

    @property
    def z_dot(self):
        return self.X[8]

    # body rates [rad / s] (in body frame)
    @property
    def p(self):
        return self.X[9]

    @property
    def q(self):
        return self.X[10]

    @property
    def r(self):
        return self.X[11]

    # forces from the four propellers
    @property
    def f_1(self):
        f = self.k_f * self.omega[0] ** 2
        return f

    @property
    def f_2(self):
        f = self.k_f * self.omega[1] ** 2
        return f

    @property
    def f_3(self):
        f = self.k_f * self.omega[2] ** 2
        return f

    @property
    def f_4(self):
        f = self.k_f * self.omega[3] ** 2
        return f

    # collective force
    @property
    def f_total(self):
        f_t = self.f_1 + self.f_2 + self.f_3 + self.f_4
        return f_t

    # Reactive moments 1 through 4
    @property
    def tau_1(self):
        tau = self.k_m * self.omega[0] ** 2
        return tau

    @property
    def tau_2(self):
        tau = -self.k_m * self.omega[1] ** 2
        return tau

    @property
    def tau_3(self):
        tau = self.k_m * self.omega[2] ** 2
        return tau

    @property
    def tau_4(self):
        tau = -self.k_m * self.omega[3] ** 2
        return tau

    @property
    def tau_x(self):
        tau = self.l * (self.f_1 + self.f_4 - self.f_2 - self.f_3)
        return tau

    @property
    def tau_y(self):
        tau = self.l * (self.f_1 + self.f_2 - self.f_3 - self.f_4)
        return tau

    @property
    def tau_z(self):
        tau = self.tau_1 + self.tau_2 + self.tau_3 + self.tau_4
        return tau

    def set_propeller_angular_velocities(
            self,
            i,
            j,
            c,
            u_bar_p,
            u_bar_q,
            u_bar_r):

        c_bar = -c * self.m / self.k_f
        p_bar = u_bar_p * self.i_x / (self.k_f * self.l)
        q_bar = u_bar_q * self.i_y / (self.k_f * self.l)
        r_bar = u_bar_r * self.i_z / self.k_m

        omega_4 = (c_bar + p_bar - r_bar - q_bar) / 4
        omega_3 = (r_bar - p_bar) / 2 + omega_4
        omega_2 = (c_bar - p_bar) / 2 - omega_3
        omega_1 = c_bar - omega_2 - omega_3 - omega_4

        if omega_1 < 0 or omega_2 < 0 or omega_3 < 0 or omega_4 < 0:
            print("ERROR - i:{}, j:{}".format(i, j))
            print("(1)omega_1:{}, omega_2:{}, omega_3:{}, omega_4:{}".format(omega_1, omega_2, omega_3, omega_4))
            sys.exit()

        elif np.isnan(omega_1) or np.isnan(omega_2) or np.isnan(omega_3) or np.isnan(omega_4):
            print("ERROR - i:{}, j:{}".format(i, j))
            print("(2)omega_1:{}, omega_2:{}, omega_3:{}, omega_4:{}".format(omega_1, omega_2, omega_3, omega_4))
            sys.exit()

        else:
            self.omega[0] = -np.sqrt(omega_1)
            self.omega[1] = np.sqrt(omega_2)
            self.omega[2] = -np.sqrt(omega_3)
            self.omega[3] = np.sqrt(omega_4)

    def get_rotation_matrix(self):
        r_x = np.array([[1, 0, 0],
                        [0, cos(self.phi), -sin(self.phi)],
                        [0, sin(self.phi), cos(self.phi)]])

        r_y = np.array([[cos(self.theta), 0, sin(self.theta)],
                        [0, 1, 0],
                        [-sin(self.theta), 0, cos(self.theta)]])

        r_z = np.array([[cos(self.psi), -sin(self.psi), 0],
                        [sin(self.psi), cos(self.psi), 0],
                        [0, 0, 1]])

        r = np.matmul(r_z, np.matmul(r_y, r_x))
        return r

    def get_linear_acceleration(self):
        R = self.get_rotation_matrix()
        g = np.array([0, 0, self.g]).T
        c = -self.f_total
        accelerations = g + np.matmul(R, np.array([0, 0, c]).T) / self.m
        return accelerations

    def get_omega_dot(self):
        p_dot = self.tau_x / self.i_x - self.r * self.q * (self.i_z - self.i_y) / self.i_x
        q_dot = self.tau_y / self.i_y - self.r * self.p * (self.i_x - self.i_z) / self.i_y
        r_dot = self.tau_z / self.i_z - self.q * self.p * (self.i_y - self.i_x) / self.i_z

        return np.array([p_dot, q_dot, r_dot])

    def get_euler_derivatives(self):
        euler_rot_mat = np.array([[1, sin(self.phi) * tan(self.theta), cos(self.phi) * tan(self.theta)],
                                  [0, cos(self.phi), -sin(self.phi)],
                                  [0, sin(self.phi) / cos(self.theta), cos(self.phi) / cos(self.theta)]])

        derivatives_in_bodyframe = np.array([self.p, self.q, self.r]).T

        euler_dot = np.matmul(euler_rot_mat, derivatives_in_bodyframe)

        return euler_dot

    def advance_state(self, dt):
        euler_dot_lab = self.get_euler_derivatives()
        body_frame_angle_dot = self.get_omega_dot()
        accelerations = self.get_linear_acceleration()

        x_dot = np.array([self.X[6],
                          self.X[7],
                          self.X[8],
                          euler_dot_lab[0],
                          euler_dot_lab[1],
                          euler_dot_lab[2],
                          accelerations[0],
                          accelerations[1],
                          accelerations[2],
                          body_frame_angle_dot[0],
                          body_frame_angle_dot[1],
                          body_frame_angle_dot[2]])

        self.X = self.X + x_dot * dt

    def get_state(self):
        return self.X

    def set_state(self, x):
        self.X = x

