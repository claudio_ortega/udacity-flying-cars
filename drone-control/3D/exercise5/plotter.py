import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as Axes3D


def plot_all(
        t_path,
        x_path,
        y_path,
        z_path,
        psi_path,
        drone_state_history,
        omega_history):

    x_actual = drone_state_history[:, 0]
    y_actual = drone_state_history[:, 1]
    z_actual = drone_state_history[:, 2]
    psi_actual = drone_state_history[:, 5]

    #
    fig = plt.figure(figsize=(20, 10))

    # 1
    ax1 = fig.add_subplot(331, projection='3d')

    set_limits(ax1, x_path, y_path, z_path)
    set_labels(ax1, "x", "y", "z")

    ax1.plot(
        x_path,
        y_path,
        z_path,
        lw=3.0, alpha=.5, color='red')

    ax1.plot(
        x_actual,
        y_actual,
        z_actual,
        lw=1.0, alpha=1.0, color='blue')

    plt.legend(
        ['target position', 'actual position'],
        fontsize=10)

    # 4
    ax4 = fig.add_subplot(334)

    ax4.plot(t_path, x_path)
    ax4.plot(t_path, x_actual)
    ax4.set_xlabel("time")
    ax4.set_ylabel("x")

    plt.legend(
        ['target x', 'actual x'],
        fontsize=10)
    
    # 5
    ax5 = fig.add_subplot(335)

    ax5.plot(t_path, y_path)
    ax5.plot(t_path, y_actual)
    ax5.set_xlabel("time")
    ax5.set_ylabel("y")

    plt.legend(
        ['target y', 'actual y'],
        fontsize=10)

    # 6
    ax6 = fig.add_subplot(336)

    ax6.plot(t_path, z_path)
    ax6.plot(t_path, z_actual)
    ax6.set_xlabel("time")
    ax6.set_ylabel("z")

    plt.legend(
        ['target z', 'actual z'],
        fontsize=10)

    # 7
    err = np.sqrt((x_path - x_actual) ** 2 + (y_path - y_actual) ** 2 + (z_path - z_actual) ** 2)

    ax7 = fig.add_subplot(337)

    ax7.plot(t_path, err)
    ax7.set_xlabel("time")
    ax7.set_ylabel("meters")

    plt.legend(
        ['position error'],
        fontsize=10)

    # 8
    ax8 = fig.add_subplot(338)

    ax8.plot(t_path, -omega_history[:, 0], color='blue')
    ax8.plot(t_path, omega_history[:, 1], color='red')
    ax8.plot(t_path, -omega_history[:, 2], color='green')
    ax8.plot(t_path, omega_history[:, 3], color='black')

    ax8.set_xlabel("t")
    ax8.set_ylabel("prop omega")

    plt.legend(
        ['p1', 'p2', 'p3', 'p4'],
        fontsize=10)

    # 9
    ax9 = fig.add_subplot(339)

    ax9.plot(t_path, psi_path, color='blue')
    ax9.plot(t_path, psi_actual, color='green')

    ax9.set_xlabel("t")
    ax9.set_ylabel("yaw angle - psi")

    plt.legend(
        ['commanded', 'actual'],
        fontsize=10)

    #
    plt.show()


def set_limits(axis, np_array_x, np_array_y, np_array_z):
    axis.set_xlim(np.amin(np_array_x), np.amax(np_array_x))
    axis.set_ylim(np.amin(np_array_y), np.amax(np_array_y))
    axis.set_zlim(np.amin(np_array_z), np.amax(np_array_z))


def set_labels(axis, xlabel, ylabel, zlabel):
    axis.set_xlabel(xlabel)
    axis.set_ylabel(ylabel)
    axis.set_zlabel(zlabel)

