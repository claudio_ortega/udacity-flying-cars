import numpy as np
from numpy.polynomial.polynomial import polyval
from common import multiple_waypoints, rhs_generation
from plot import plot_1D


def main():
    t = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    x = np.array([0, 1, 2, 4, 6, 7, 7.5, 6.5, 5, 4.5, 4.0])

    m = multiple_waypoints(t)
    b = rhs_generation(x)

    coeff = np.linalg.solve(m, b)
    c = np.zeros((int(coeff.shape[0] / 8), 8))

    for loop in range(t.size - 1):

        t1 = np.linspace(-1.0, 1.0, 101)
        x1 = np.zeros(t1.size)
        c[loop, :] = coeff[8 * loop:8 * loop + 8]

        for i in range(t1.size):
            x1[i] = polyval(t1[i], c[loop, :])

        if loop == 0:
            xx = x1[:-1]
            tx = (t[loop] + t[loop + 1]) / 2 + t1 * (t[loop + 1] - t[loop]) / 2
            tt = tx[:-1]
        else:
            xx = np.hstack((xx, x1[:-1]))
            tx = (t[loop] + t[loop + 1] + t1 * (t[loop + 1] - t[loop])) / 2
            tt = np.hstack((tt, tx[:-1]))

    v = np.diff(xx)
    a = np.diff(v)
    j = np.diff(a)
    s = np.diff(j)

    plot_1D(tt, xx, v, a, j, s)


if __name__ == "__main__":
    main()



