import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as Axes3D


def plot_1D (t, p, v, a, j, s):

    plt.figure(figsize=(10, 5))

    plt.subplot(2,3,1)
    plt.plot(t, p)
    plt.legend(['Position'],fontsize = 10)
    plt.xlabel('$t$ [$s$]').set_fontsize(10)
    plt.ylabel('$\{x}$ [$m$]').set_fontsize(10)

    plt.subplot(2,3,2)
    plt.plot(t[:-1], v)
    plt.legend(['Velocity'],fontsize = 10)
    plt.xlabel('$t$ [$s$]').set_fontsize(10)
    plt.ylabel('$\dot{x}$ [$m/s$]').set_fontsize(10)

    plt.subplot(2,3,3)
    plt.plot(t[:-2], a)
    plt.legend(['Acceleration'],fontsize = 10)
    plt.xlabel('$t$ [$s$]').set_fontsize(10)
    plt.ylabel('$\ddot{x}$ [$m/s^2$]').set_fontsize(10)

    plt.subplot(2,3,4)
    plt.plot(t[:-3], j)
    plt.legend(['Jerk'],fontsize = 10)
    plt.xlabel('$t$ [$s$]').set_fontsize(10)
    plt.ylabel('$x^{(3)}$ [$m/s^3$]').set_fontsize(10)

    plt.subplot(2,3,5)
    plt.plot(t[:-4], s)
    plt.legend(['Snap'],fontsize = 10)
    plt.xlabel('$t$ [$s$]').set_fontsize(10)
    plt.ylabel('$x^{(4)}$ [$m/s^4$]').set_fontsize(10)

    plt.show()


def plot_3D (x, y, z, p):

    fig = plt.figure(figsize=(10, 5))
    ax = fig.gca(projection='3d')
    ax.plot(x, y, z)
    ax.scatter(p[0, :], p[1, :], p[2, :], marker='o', color='red')

    plt.title('Flight path').set_fontsize(20)
    ax.set_xlabel('$x$ [$m$]').set_fontsize(20)
    ax.set_ylabel('$y$ [$m$]').set_fontsize(20)
    ax.set_zlabel('$z$ [$m$]').set_fontsize(20)
    plt.legend(['Planned path', 'waypoints'], fontsize=14)

    plt.show()

