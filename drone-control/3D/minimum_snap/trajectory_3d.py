import numpy as np
from numpy.polynomial.polynomial import polyval
from common import multiple_waypoints, rhs_generation


def compute_sinusoidal_trajectory(number_points, amplitude=1):

    total_time = 20.0
    t_path = np.linspace(0.0, total_time, number_points)

    total_cycles = 2
    phi = 0
    az = 0 * amplitude
    bz = 1 * amplitude
    omega_x_f = 1

    omega = total_cycles * 2 * np.pi / total_time

    omega_x = omega_x_f * omega
    omega_y = omega
    x_path = amplitude * np.sin(omega_x * t_path + phi)
    y_path = amplitude * np.cos(omega_y * t_path + phi)

    omega_z = omega
    z_path = az * np.cos(omega_z * t_path + phi) + bz * (t_path / 5)

    return t_path, x_path, y_path, z_path


def compute_minimum_snap_trajectory(wp_t, wp_p, number_points):

    m = multiple_waypoints(wp_t)

    b_x = rhs_generation(wp_p[0, :])
    b_y = rhs_generation(wp_p[1, :])
    b_z = rhs_generation(wp_p[2, :])

    coeff_x = np.linalg.solve(m, b_x)
    coeff_y = np.linalg.solve(m, b_y)
    coeff_z = np.linalg.solve(m, b_z)

    c_x = np.zeros((int(coeff_x.size / 8), 8))
    c_y = np.zeros((int(coeff_x.size / 8), 8))
    c_z = np.zeros((int(coeff_x.size / 8), 8))

    t1 = np.linspace(-1.0, 1.0, number_points + 1)

    t_path = np.zeros(t1.size)
    x_path = np.zeros(t1.size)
    y_path = np.zeros(t1.size)
    z_path = np.zeros(t1.size)

    for loop in range(wp_t.size - 1):

        x1 = np.zeros(t1.size)
        y1 = np.zeros(t1.size)
        z1 = np.zeros(t1.size)

        c_x[loop, :] = coeff_x[8 * loop:8 * loop + 8]
        c_y[loop, :] = coeff_y[8 * loop:8 * loop + 8]
        c_z[loop, :] = coeff_z[8 * loop:8 * loop + 8]

        for i in range(t1.size):
            x1[i] = polyval(t1[i], c_x[loop, :])
            y1[i] = polyval(t1[i], c_y[loop, :])
            z1[i] = polyval(t1[i], c_z[loop, :])

        if loop == 0:
            x_path = x1[:-1]
            y_path = y1[:-1]
            z_path = z1[:-1]
            tx = (wp_t[loop] + wp_t[loop + 1]) / 2 + t1 * (wp_t[loop + 1] - wp_t[loop]) / 2
            t_path = tx[:-1]
        else:
            x_path = np.hstack((x_path, x1[:-1]))
            y_path = np.hstack((y_path, y1[:-1]))
            z_path = np.hstack((z_path, z1[:-1]))
            tx = (wp_t[loop] + wp_t[loop + 1] + t1 * (wp_t[loop + 1] - wp_t[loop])) / 2
            t_path = np.hstack((t_path, tx[:-1]))

    return t_path, x_path, y_path, z_path


def compute_2_derivatives_3D(t_path, x_path, y_path, z_path):

    x_dot_path, x_dot_dot_path = compute_2_derivatives_1D(t_path, x_path)
    y_dot_path, y_dot_dot_path = compute_2_derivatives_1D(t_path, y_path)
    z_dot_path, z_dot_dot_path = compute_2_derivatives_1D(t_path, z_path)

    return (
        x_dot_path, y_dot_path, z_dot_path,
        x_dot_dot_path, y_dot_dot_path, z_dot_dot_path)


def compute_2_derivatives_1D(t_path, x_path):

    x_dot_path = compute_first_derivative_1D(x_path, t_path)
    x_dot_dot_path = compute_first_derivative_1D(x_dot_path, t_path)

    return x_dot_path, x_dot_dot_path


def compute_first_derivative_1D(f_path, t_path):
    t_dot_path = np.diff(t_path)
    f_dot_path_tmp = np.diff(f_path)
    f_dot_path = np.divide(f_dot_path_tmp, t_dot_path)

    # in order to keep same length on the return array,
    # we need to produce an extra sample,
    # we'll extrapolate the last sample linearly from the previous two
    second_last = f_dot_path[-2]
    last = f_dot_path[-1]
    new_last = last + (last - second_last)

    f_dot_path = np.append(f_dot_path, new_last)
    return f_dot_path

