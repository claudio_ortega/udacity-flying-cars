import numpy as np
from plot import plot_3D
from trajectory_3d import compute_minimum_snap_trajectory


def main():

    waypoint_points = np.array(
       [[0, 0, 0],
        [1, 0, 1],
        [2, 0, 1],
        [2, 2, 1],
        [1.5, 1.5, 1.5],
        [1.5, 1.5, 2.0]]).T

    number_samples = 1000

    target_t, target_x, target_y, target_z = compute_minimum_snap_trajectory(
        np.array([0, 1, 2, 3, 4, 5]),
        waypoint_points,
        number_samples)

    plot_3D(target_x, target_y, target_z, waypoint_points)


if __name__ == "__main__":
    main()



