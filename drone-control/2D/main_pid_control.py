from monorotor import MonoRotor
from controllers import PIDController
from plotting import plot_signals
from trajectories import step


def main():

    MASS_ERROR = 1.0

    SIGMA = 0.7
    T = 2

    K_D = ( 1 + ( 2 * SIGMA ) ) / T
    K_P = K_D / T
    K_I = 1 / ( T*T*T )

    # preparation
    drone = MonoRotor()
    perceived_mass = drone.m * MASS_ERROR
    controller = PIDController(K_P, K_D, K_I, perceived_mass)

    dt = 0.001
    t, z_path, z_dot_path = step(to=-1, duration=15.0, dt=dt)

    # run simulation
    history = []
    for z_target, z_dot_target in zip(z_path, z_dot_path):

        z_actual = drone.z
        z_dot_actual = drone.z_dot

        u = controller.thrust_control(
            z_target,
            z_actual,
            z_dot_target,
            z_dot_actual,
            dt,
            0)

        drone.thrust = u
        drone.advance_state(dt)
        history.append(drone.X)

    # generate plots
    z_actual = [h[0] for h in history]
    plot_signals(
        t,
        [z_path, z_actual],
        ['target', 'actual'],
        ['red', 'blue'],
        [1, 1],
        'PID'
    )


if __name__ == "__main__":
    main()
