import numpy as np

from monorotor import MonoRotor
from controllers import OpenLoopController
from plotting import plot_signals


def main():

    total_time = 10
    time_line = np.linspace(0.0, total_time, 1000)
    dt = time_line[1] - time_line[0]
    z_target = 0.5*np.cos(2*time_line)-0.5

    drone = MonoRotor()

    drone_start_state = drone.X
    drone_state_history = []

    # The mass that the controller believes is not necessarily the
    # true mass of the drone. This reflects the real world more accurately.
    mass_error = 1.01
    controller = OpenLoopController(drone.m * mass_error, drone_start_state)

    # 2. Run the simulation
    for target_z in z_target:
        drone_state_history.append(drone.X)
        drone.thrust = controller.thrust_control(target_z, dt)
        drone.advance_state(dt)

    # 3. Generate plots
    z_actual = [h[0] for h in drone_state_history]
    plot_signals(
        time_line,
        [z_target, z_actual],
        ['target', 'actual'],
        ['red', 'blue'],
        [1, 1],
        "open loop"
    )

if __name__ == "__main__":
    main()
