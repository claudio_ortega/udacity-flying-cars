# This code simulates TWO drones. One uses the feed forward
# acceleration and the other doesn't. Note the difference in
# trajectories.

from monorotor import MonoRotor
from controllers import PDController
from plotting import plot_signals
import numpy as np
from trajectories import cosine


def main():

    MASS_ERROR = 1.0
    K_P = 100.0
    K_D = 2.0

    AMPLITUDE = 0.5
    OSCILLATION_FREQUENCY = 1e-6 #5

    PERIOD = 2 * np.pi / OSCILLATION_FREQUENCY

    # preparation (TWO drones to compare)
    drone = MonoRotor()
    ff_drone = MonoRotor()
    perceived_mass = drone.m * MASS_ERROR

    # instantiate TWO controllers
    controller = PDController(K_P, K_D, perceived_mass)
    ff_controller = PDController(K_P, K_D, perceived_mass)

    # get trajectories
    t, z_path, z_dot_path, z_dot_dot_path = cosine(
        AMPLITUDE,
        PERIOD,
        duration=6.0)

    dt = t[1] - t[0]
    # run simulation
    history = []
    ff_history = []

    for z_target, z_dot_target, z_dot_dot_ff in zip(
            z_path,
            z_dot_path,
            z_dot_dot_path):

        z_actual = drone.z
        z_dot_actual = drone.z_dot

        ff_z_actual = ff_drone.z
        ff_z_dot_actual = ff_drone.z_dot

        u_ff = ff_controller.thrust_control(
            z_target,
            ff_z_actual,
            z_dot_target,
            ff_z_dot_actual,
            z_dot_dot_ff)

        u = controller.thrust_control(
            z_target,
            z_actual,
            z_dot_target,
            z_dot_actual,
            0)

        drone.thrust = u
        ff_drone.thrust = u_ff

        drone.advance_state(dt)
        ff_drone.advance_state(dt)

        history.append(drone.X)
        ff_history.append(ff_drone.X)

    # generate plots
    z_actual = [h[0] for h in history]
    z_ff_actual = [h[0] for h in ff_history]
    plot_signals(
        t,
        [z_path, z_actual, z_ff_actual],
        ['target', 'actual', 'ff_actual'],
        ['red', 'blue', 'green'],
        [1, 1, 1],
        "feed forward"
    )


if __name__ == "__main__":
    main()
