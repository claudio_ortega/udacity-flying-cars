import numpy as np

from monorotor import MonoRotor
from controllers import PController
from plotting import plot_signals


def main():

    total_time = 10
    dt = 0.001
    time_line = np.linspace(0.0, total_time, int(total_time / dt))
    z_target = np.ones(time_line.shape[0])

    drone = MonoRotor()

    # The mass that the controller believes is not necessarily the
    # true mass of the drone. This reflects the real world more accurately.
    mass_error = 1.01
    kp = 10.0

    controller = PController(kp, drone.m * mass_error)

    # run simulation
    history = []
    for z_target_next in z_target:

        z_actual_next = drone.z

        drone.thrust = controller.thrust_control(z_target_next, z_actual_next)
        drone.advance_state(dt)

        history.append(drone.X)

    # 3. Generate plots
    z_actual = [h[0] for h in history]
    plot_signals(
        time_line,
        [z_target, z_actual],
        ['target', 'actual'],
        ['red', 'blue'],
        [1, 1],
        "P"
    )

if __name__ == "__main__":
    main()
