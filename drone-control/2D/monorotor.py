import numpy as np


"""
   this IS the idealized model for a quadrotor with only one degree of freedom: 
   the Z coordinate 
   
   z,z-dot and z-dot-dot in this model correspond with the actual Z coordinate,
   so IOW this is modeling the dynamics of the quad that can only 
   move up (Z negative) or move down (Z positive)
   
   it only memorizes Z,Z-dot, and computes Z-d--dot from the newton equation of movement 
   F=ma
"""


class MonoRotor:

    def __init__(self, m=1.0):
        self.m = m
        self.g = 9.81

        self.thrust = 0.0

        # z, z_dot
        self.X = np.array([0.0, 0.0])

    @property
    def z(self):
        return self.X[0]

    @property
    def z_dot(self):
        return self.X[1]

    @property
    def z_dot_dot(self):
        f_net = self.m * self.g - self.thrust
        return f_net / self.m

    def advance_state(self, dt):
        X_dot = np.array([
            self.z_dot,
            self.z_dot_dot])

        self.X = self.X + X_dot * dt
        return self.X
