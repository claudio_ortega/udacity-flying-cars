from trajectories import figure_8
from drone_2d import Drone2D
from cascaded_controllers import NonLinearCascadingController
from simulate import zy_flight
from plotting import plot_zy_flight_path


def main():

    z_k_p = 0.1
    z_k_d = 10.0
    y_k_p = 0.3
    y_k_d = 10.0
    phi_k_p = 50.0
    phi_k_d = 50.0

    drone = Drone2D()

    # INSTANTIATE CONTROLLER
    controller = NonLinearCascadingController(
        drone.m,
        drone.I_x,
        z_k_p=z_k_p,
        z_k_d=z_k_d,
        y_k_p=y_k_p,
        y_k_d=y_k_d,
        phi_k_p=phi_k_p,
        phi_k_d=phi_k_d
    )

    # TRAJECTORY PARAMETERS (you don't need to change these)
    total_time = 30.0
    omega_z = 1.0  # angular frequency of figure 8

    # GENERATE FIGURE 8
    z_traj, y_traj, t = figure_8(omega_z, total_time, dt=0.02)
    z_path, z_dot_path, z_dot_dot_path = z_traj
    y_path, y_dot_path, y_dot_dot_path = y_traj

    # SIMULATE MOTION
    linear_history = zy_flight(
        z_traj,
        y_traj,
        t,
        controller,
        inner_loop_speed_up=10)

    plot_zy_flight_path(z_path, y_path, linear_history)


if __name__ == "__main__":
    main()
