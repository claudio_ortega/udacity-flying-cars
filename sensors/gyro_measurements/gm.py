import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab


def measure_turn_rate(
        omega,  # The true value of the omega
        bias,  # Value of the bias term
        sigma_omega  # Sigma value associated with the measurement
        ):
    return omega + bias + np.random.normal(0.0, sigma_omega)  # None


def calculate_attitude(
        true_angle,  # The true value of the angular velocity
        measured_angle,  # Previously estimated angle by integration
        accumulated_sigma,  # Accumulated effective sigma due to integration
        omega,  # The true omega
        measured_omega,  # Measured omega
        sigma_omega,  # The uncertainty of each measurement
        dt):  # The time interval between measurements

    true_angle = true_angle + omega * dt
    measured_angle = measured_angle + measured_omega * dt
    accumulated_sigma = np.sqrt(accumulated_sigma ** 2 + dt ** 2 * sigma_omega ** 2)

    return true_angle, measured_angle, accumulated_sigma


def main():
    omega = 0.2  # The true angular velocity [rad/sec]
    total_time = 10  # Total time of the measurements [sec]
    dt = 0.01  # Time interval between measurements [sec]
    sigma_omega = 0.1  # The sigma value for the Gaussian distribution associated with the measurement
    bias = -0.01  # The bias of the measurement unit (5%)

    # An array of time value for each measurement
    t = np.linspace(0.0, total_time, int(total_time / dt))

    # Initial values of the true turn angle, measured angle is initialized at 0 as well as
    # the initial value of the accumulated sigma.
    true_angle, measured_angle, accumulated_sigma = 0.0, 0.0, 0.0

    # Initializing the arrays for historical values for each variable
    true_angle_history = np.array([true_angle])
    measured_angle_history = np.array([true_angle])
    accumulated_sigma_history = np.array([accumulated_sigma])
    measured_omega_history = np.array([omega])

    for i in range(1, t.shape[0]):
        # Simulating the angular velocity measurement
        measured_omega = measure_turn_rate(omega, bias, sigma_omega)

        # Advancing the true turn angle, measured angle, and the accumulated sigma
        # in time by new measurement values.
        true_angle, measured_angle, accumulated_sigma = calculate_attitude(
            true_angle,
            measured_angle,
            accumulated_sigma,
            omega,
            measured_omega,
            sigma_omega,
            dt)

        # Adding the returned values to the historical array
        measured_omega_history = np.vstack((measured_omega_history, measured_omega))
        true_angle_history = np.vstack((true_angle_history, true_angle))
        measured_angle_history = np.vstack((measured_angle_history, measured_angle))
        accumulated_sigma_history = np.vstack((accumulated_sigma_history, accumulated_sigma))

    pylab.rcParams['figure.figsize'] = 8,8
    plt.plot(t, true_angle_history, color='green')
    plt.plot(t, measured_angle_history, color='blue')
    plt.plot(t, true_angle_history+3*accumulated_sigma_history, color='red')
    plt.plot(t, true_angle_history-3*accumulated_sigma_history, color='red')
    plt.title('Angular position').set_fontsize(20)
    plt.ylabel('$\phi$ [$rad$]').set_fontsize(20)
    plt.xlabel('$t$ [$sec$]').set_fontsize(20)
    plt.xticks(fontsize = 14)
    plt.yticks(fontsize = 14)
    plt.legend(['Actual angle','Measured angle','Actual angle $+ 3 \sigma$','Actual angle $- 3 \sigma$'],fontsize = 14)
    plt.show()


if __name__ == "__main__":
    main()


