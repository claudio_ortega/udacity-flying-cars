import numpy as np
import matplotlib.pyplot as plt
import time
from shapely.geometry import Polygon, Point
from grid import create_grid


def main():

    plt.rcParams["figure.figsize"] = [6, 6]

    plt.xlabel('EAST')
    plt.ylabel('NORTH')

    filename = 'data/colliders.csv'
    # Read in the data skipping the first two lines.
    # Note: the first line contains the latitude and longitude of map center
    # Where is this??
    data = np.loadtxt(filename, delimiter=',', dtype='Float64', skiprows=2)
    print("lData:\n{}".format(data))

    # polygon extraction
    polygons, xmin, xmax, ymin, ymax, _, _ = extract_polygons(data)
    zmin = 0
    zmax = 10

    print("x: min = {0}, max = {1}".format(xmin, xmax))
    print("y: min = {0}, max = {1}".format(ymin, ymax))
    print("z: min = {0}, max = {1}".format(zmin, zmax))
    print("polygons.len:\n{}".format(len(polygons)))
    for poly, height in polygons:
        print("poly:{0}, height:{1}".format(poly, height))
        break

    num_samples = 100
    samples = sample_prism( num_samples, xmin, xmax, ymin, ymax, zmin, zmax)

    print("samples.len:{}".format(len(samples)))
    for x in samples:
        print("x:{}".format(x))
        break

    t0 = time.time()
    non_collision = eliminate_collisions( polygons, samples )
    time_taken = time.time() - t0
    print("Time taken {} seconds ...".format(time_taken))
    print("to_keep.len:{}".format(len(non_collision)))

    grid = create_grid(data, zmax, 1)

    plt.imshow(grid, cmap='Greys', origin='lower')

    # draw points
    nmin = np.min(data[:, 0])
    emin = np.min(data[:, 1])
    all_pts = np.array(non_collision)
    north_vals = all_pts[:,0]
    east_vals = all_pts[:,1]
    plt.scatter(east_vals - emin, north_vals - nmin, c='red')

    plt.ylabel('NORTH')
    plt.xlabel('EAST')

    plt.show()


def sample_prism( num_samples, xmin, xmax, ymin, ymax, zmin, zmax ):
    xvals = np.random.uniform(xmin, xmax, num_samples)
    yvals = np.random.uniform(ymin, ymax, num_samples)
    zvals = np.random.uniform(zmin, zmax, num_samples)
    samples = np.array(list(zip(xvals, yvals, zvals)))
    return samples


def extract_polygons(data):
    polygons = []
    for i in range(data.shape[0]):
        north, east, alt, d_north, d_east, d_alt = data[i, :]

        # NOTE: The order of the points needs to be counterclockwise
        # in order to work with the simple angle test
        # Also, `shapely` draws sequentially from point to point.
        #
        # If the area of the polygon in shapely is 0
        # you've likely got a weird order.
        obstacle = [north - d_north, north + d_north, east - d_east, east + d_east]
        corners = [(obstacle[0], obstacle[2]), (obstacle[0], obstacle[3]), (obstacle[1], obstacle[3]),
                   (obstacle[1], obstacle[2])]

        height = alt + d_alt

        p = Polygon(corners)
        polygons.append((p, height))

    xmin = np.min(data[:, 0] - data[:, 3])
    xmax = np.max(data[:, 0] + data[:, 3])
    ymin = np.min(data[:, 1] - data[:, 4])
    ymax = np.max(data[:, 1] + data[:, 4])
    zmin = np.min(data[:, 2] - data[:, 5])
    zmax = np.max(data[:, 2] + data[:, 5])

    return polygons, xmin, xmax, ymin, ymax, zmin, zmax


def eliminate_collisions ( polygons, samples ):
    non_collision = []
    for point in samples:
        if not contains(polygons, point):
            non_collision.append(point)
    return non_collision


def contains(polygons, point):
    for (p, height) in polygons:
        if p.contains(Point(point)) and height >= point[2]:
            return True
    return False


if __name__ == "__main__":
    main()
