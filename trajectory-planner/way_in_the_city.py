import numpy as np
import matplotlib.pyplot as plt
from grid import create_grid
from a_star import compute_a_star


def main():

    np.set_printoptions(precision=6)

    plt.rcParams["figure.figsize"] = [6, 6]

    filename = 'data/colliders.csv'
    # Read in the data skipping the first two lines.
    # Note: the first line contains the latitude and longitude of map center
    # Where is this??
    lData = np.loadtxt(filename, delimiter=',', dtype='Float64', skiprows=2)
    print("lData:{}".format(lData))

    lDrone_altitude = 5
    lSafe_distance = 3

    lGrid = create_grid(lData, lDrone_altitude, lSafe_distance)

    start_ne = (25, 100)
    goal_ne = (750., 370.)

    print("computing a-star -- BEGIN")
    path, cost, found = compute_a_star(lGrid, heuristic_manhattan, start_ne, goal_ne)
    print("computing a-star -- END")

    print("len(path):{}".format(len(path)))
    print("cost:{}".format(cost))

    pruned_path = prune_path(path)
    print("len(pruned_path):{}".format(len(pruned_path)))

    plt.imshow(lGrid, origin='lower')
    plt.xlabel('EAST')
    plt.ylabel('NORTH')

    # For the purposes of the visual the east coordinate lay along
    # the x-axis and the north coordinates long the y-axis.
    plt.plot(start_ne[1], start_ne[0], 'x')
    plt.plot(goal_ne[1], goal_ne[0], 'x')

    pp = np.array(path)

    # commenting out next line
    #plt.plot(pp[:, 1].value, pp[:, 0].value, 'g')
    #
    # gives this error:
    #   IndexError: too many indices for array
    #   plt.plot(pp[:, 1], pp[:, 0], 'g')

    i = 0
    while i < len(pruned_path):
        print("i:{}".format(i) )
        print("value:{}".format(pruned_path[i].value))
        plt.plot(pruned_path[i].value[1], pruned_path[i].value[0], 'g')
        i = i+1

    print("showing map, close the map for exiting")
    plt.show()


def heuristic_manhattan(position, goal_position):
    return np.abs(position[0] - goal_position[0]) + np.abs(position[1] - goal_position[1])


def point(p):
    return np.array([p[0], p[1], 1.]).reshape(1, -1)


def collinearity_check(p1, p2, p3, epsilon=1e-6):
    m = np.concatenate((p1, p2, p3), 0)
    det = np.linalg.det(m)
    return abs(det) < epsilon


# We're using co-linearity here, but you could use Bresenham as well!
def prune_path(path):

    pruned_path = [p for p in path]

    i = 0
    while i < len(pruned_path) - 2:
        p1 = point(pruned_path[i].value)
        p2 = point(pruned_path[i + 1].value)
        p3 = point(pruned_path[i + 2].value)

        # If the 3 points are in a line remove
        # the 2nd point.
        # The 3rd point now becomes and 2nd point
        # and the check is redone with a new third point
        # on the next iteration.
        if collinearity_check(p1, p2, p3):
            # Something subtle here but we can mutate
            # `pruned_path` freely because the length
            # of the list is check on every iteration.
            pruned_path.remove(pruned_path[i + 1])
        else:
            i += 1

    return pruned_path


if __name__ == "__main__":
    main()
