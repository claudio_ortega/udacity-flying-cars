import numpy as np
from enum import Enum
from queue import Queue

class Action(Enum):
    LEFT = (0, -1, 1)
    RIGHT = (0, 1, 1)
    UP = (-1, 0, 1)
    DOWN = (1, 0, 1)

    def __str__(self):
        if self == self.LEFT:
            return '<'
        elif self == self.RIGHT:
            return '>'
        elif self == self.UP:
            return '^'
        elif self == self.DOWN:
            return 'v'


# Define a function that returns a list of valid actions from the current node
def valid_actions(agrid, current_node):

    valid = [Action.UP, Action.LEFT, Action.RIGHT, Action.DOWN]
    n, m = agrid.shape[0] - 1, agrid.shape[1] - 1
    x, y = current_node

    # check if the adjacent node are either off the grid or an obstacle, removing forbidden moves
    if x - 1 < 0 or agrid[x - 1, y] == 1:
        valid.remove(Action.UP)
    if x + 1 > n or agrid[x + 1, y] == 1:
        valid.remove(Action.DOWN)
    if y - 1 < 0 or agrid[x, y - 1] == 1:
        valid.remove(Action.LEFT)
    if y + 1 > m or agrid[x, y + 1] == 1:
        valid.remove(Action.RIGHT)

    return valid


# Define a function to visualize the path
def visualize_path(grid, path, start):
    """
    Given a grid, path and start position
    return visual of the path to the goal.

    'S' -> start
    'G' -> goal
    'O' -> obstacle
    ' ' -> empty
    """
    # Define a grid of string characters for visualization
    sgrid = np.zeros(np.shape(grid), dtype=np.str)
    sgrid[:] = ' '
    sgrid[grid[:] == 1] = '*'

    pos = start
    # Fill in the string grid
    for a in path:
        da = a.value
        sgrid[pos[0], pos[1]] = str(a)
        pos = (pos[0] + da[0], pos[1] + da[1])
    sgrid[pos[0], pos[1]] = 'G'
    sgrid[start[0], start[1]] = 'S'
    return sgrid


def breadth_first(grid, start, goal):

    next_to_visit = Queue()
    visited_nodes = set()
    next_in_shortest_path = {}
    found = False

    visited_nodes.add(start)
    next_to_visit.put((0, start))

    while not next_to_visit.empty():

        current_item = next_to_visit.get()
        current_cost = current_item[0]
        current_node = current_item[1]

        print("current_item:{},current_cost:{},".format(
            current_item,
            current_cost))

        if current_node == goal:
            print('found the goal !!!')
            found = True
            break

        else:
            for action in valid_actions(grid, current_node):

                new_cost = current_cost + action.value[2]
                next_node = (current_node[0] + action.value[0],
                             current_node[1] + action.value[1])

                if next_node not in visited_nodes:
                    next_to_visit.put( ( new_cost, next_node ) )
                    visited_nodes.add( next_node )
                    next_in_shortest_path[next_node] = (new_cost, current_node, action)

    # retrace steps using next_in_shortest_path, and accumulate over shortest_path
    shortest_path_actions = []

    if found:
        shortest_path_actions.append(next_in_shortest_path[goal][2])

        tmp = goal
        while next_in_shortest_path[tmp][1] != start:
            tmp = next_in_shortest_path[tmp][1]
            shortest_path_actions.append(next_in_shortest_path[tmp][2])

    return shortest_path_actions[::-1], next_in_shortest_path[goal][0]


if __name__ == "__main__":

    # Define a start and goal location
    start = (0, 0)
    goal = (4, 4)

    # Define your grid-based state space of obstacles and free space
    grid = np.array([
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 1, 0, 1],
        [0, 1, 0, 1, 0, 0],
        [0, 0, 0, 1, 1, 0],
        [0, 0, 0, 1, 0, 0],
    ])

    path, cost = breadth_first(grid, start, goal)

    print(cost)

    print(path)

    # S -> start, G -> goal, O -> obstacle
    print(visualize_path(grid, path, start))

