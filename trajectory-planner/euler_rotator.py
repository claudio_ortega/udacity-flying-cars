import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import numpy as np
from enum import Enum


class Rotation(Enum):
    ROLL = 0
    PITCH = 1
    YAW = 2

class EulerRotator:

    def __init__(self):
        """
        `rotations` is a list of 2-element tuples where the
        first element is the rotation kind and the second element
        is angle in degrees.

        Ex:

            [(Rotation.ROLL, 45), (Rotation.YAW, 32), (Rotation.PITCH, 55)]

        """


    def roll(self, phi):
        # returns rotation matrix for a rotation around the x axis (x coordinate invariant)
        return np.array(
            [
                [1, 0,           0],
                [0, np.cos(phi), -np.sin(phi)],
                [0, np.sin(phi), np.cos(phi)],
            ]
        )

    def pitch(self, theta):
        # returns rotation matrix for a rotation around the y axis (y coordinate invariant)
        return np.array(
            [
                [np.cos(theta),  0,  np.sin(theta)],
                [0,              1,  0 ],
                [-np.sin(theta), 0,  np.cos(theta)],
            ]
        )

    def yaw(self, psi):
        # returns rotation matrix for a rotation around the z axis (z coordinate invariant)
        return np.array(
            [
                [np.cos(psi), -np.sin(psi),  0],
                [np.sin(psi), np.cos(psi),   0],
                [0,           0,             1]
            ]
        )

    def rotationMatrix(self, phi, theta, psi):
        # rotation order is:
        #    (1) A: phi/x roll
        #    (2) B: theta(y) pitch
        #    (3) C: psi(z) yaw
        A = self.roll(phi)
        B = self.pitch(theta)
        C = self.yaw( psi )
        return np.matmul( np.matmul(C, B), A )

if __name__ == "__main__":

    np.set_printoptions(precision=3, suppress=True)

    rotator = EulerRotator()

    rotMatrix = rotator.rotationMatrix(
        np.deg2rad(25),
        np.deg2rad(75),
        np.deg2rad(90)
    )

    print("rotMatrix:\n{}:".format(rotMatrix))

    v1 = [1,0,0]
    v2 = [0,1,0]
    v3 = [0,0,1]

    v = [1,1,1]

    rv1 = [1,0,0]
    rv2 = [0,1,0]
    rv3 = [0,0,1]

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    plt.rcParams["figure.figsize"] = [12, 12]
    ax.set_xlim3d(-1, 1)
    ax.set_ylim3d(1, -1)
    ax.set_zlim3d(1, -1)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')

    # axes
    ax.quiver(0, 0, 0, 1, 0, 0, color='black', arrow_length_ratio=0.10)
    ax.quiver(0, 0, 0, 0, 1, 0, color='black', arrow_length_ratio=0.10)
    ax.quiver(0, 0, 0, 0, 0, 1, color='black', arrow_length_ratio=0.10)

    # pre-rotation vector
    ax.quiver(0, 0, 0, v[0], v[1], v[2], color='blue', arrow_length_ratio=0.10)

    # Rotated vector (shown in red)
    #ax.quiver(0, 0, 0, rv1[0], rv1[1], rv1[2], color='red', arrow_length_ratio=0.15)
    #ax.quiver(0, 0, 0, rv2[0], rv2[1], rv2[2], color='purple', arrow_length_ratio=0.15)
    #ax.quiver(0, 0, 0, rv3[0], rv3[1], rv3[2], color='green', arrow_length_ratio=0.15)

    plt.show()
