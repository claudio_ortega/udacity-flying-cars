import numpy as np
import matplotlib.pyplot as plt
from grid import create_grid


def main():

    np.set_printoptions(precision=6)

    plt.rcParams["figure.figsize"] = [6, 6]

    filename = 'data/colliders.csv'
    # Read in the data skipping the first two lines.
    # Note: the first line contains the latitude and longitude of map center
    # Where is this??
    lData = np.loadtxt(filename, delimiter=',', dtype='Float64', skiprows=2)
    print(lData)

    # Static drone altitude (metres)
    lDrone_altitude = 10

    # Minimum distance required to stay away from an obstacle (metres)
    # Think of this as padding around the obstacle.
    lSafe_distance = 3

    lGrid = create_grid(lData, lDrone_altitude, lSafe_distance)

    # equivalent to
    # plt.imshow(np.flip(grid, 0))
    plt.imshow(lGrid, origin='lower')

    plt.xlabel('EAST')
    plt.ylabel('NORTH')

    plt.show()


if __name__ == "__main__":
    main()
