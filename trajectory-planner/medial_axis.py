import numpy as np
import matplotlib.pyplot as plt
from grid import create_grid
from skimage.morphology import medial_axis
from skimage.util import invert
from a_star import compute_a_star


def main():

    plt.rcParams["figure.figsize"] = [6, 6]

    plt.xlabel('EAST')
    plt.ylabel('NORTH')

    filename = 'data/colliders.csv'
    # Read in the data skipping the first two lines.
    # Note: the first line contains the latitude and longitude of map center
    # Where is this??
    lData = np.loadtxt(filename, delimiter=',', dtype='Float64', skiprows=2)
    print("lData:{}".format(lData))

    lDrone_altitude = 5
    lSafe_distance = 3

    lGrid = create_grid(lData, lDrone_altitude, lSafe_distance)
    skeleton = medial_axis(invert(lGrid))

    plt.imshow(lGrid, origin='lower')
    plt.imshow(skeleton, origin='lower', alpha=0.7)

    start_ne = (25,  100)
    goal_ne = (650, 500)
    print("start_ne: {0}, goal_ne: {1}".format(start_ne, goal_ne))
    plt.plot(start_ne[1], start_ne[0], 'rx')
    plt.plot(goal_ne[1], goal_ne[0], 'rx')

    path2, cost2, found2 = compute_a_star(
        lGrid,
        heuristic_euclidean,
        start_ne,
        goal_ne)

    print("Path length (2) = {0}, path cost = {1}, found: {2}".format(len(path2), cost2, found2))

    pp = np.array(path2)
    plt.plot(pp[:, 1], pp[:, 0], 'g')

    skel_start, skel_goal = find_start_goal(skeleton, start_ne, goal_ne)
    print("skel_start: {0}, skel_goal: {1}".format(skel_start, skel_goal))
    plt.plot(skel_start[1], skel_start[0], 'gx')
    plt.plot(skel_goal[1], skel_goal[0], 'gx')

    path1, cost1, found1 = compute_a_star(
        invert(skeleton).astype(np.int),
        heuristic_euclidean,
        tuple(skel_start),
        tuple(skel_goal))

    print("Path length (1) = {0}, path cost = {1}, found: {2}".format(len(path1), cost1, found1))

    plt.show()


def find_start_goal(skel, start, goal):
    skel_cells = np.transpose(skel.nonzero())
    start_min_dist = np.linalg.norm(np.array(start) - np.array(skel_cells), axis=1).argmin()
    near_start = skel_cells[start_min_dist]
    goal_min_dist = np.linalg.norm(np.array(goal) - np.array(skel_cells), axis=1).argmin()
    near_goal = skel_cells[goal_min_dist]

    return near_start, near_goal


def heuristic_euclidean(position, goal_position):
    return np.sqrt((position[0] - goal_position[0])**2 + (position[1] - goal_position[1])**2)


if __name__ == "__main__":
    main()
