import numpy as np
import matplotlib.pyplot as plt
import time
from sampler import extract_polygons, eliminate_collisions, sample_prism
from shapely.geometry import LineString
from grid import create_grid
from sklearn.neighbors import KDTree

import pkg_resources
pkg_resources.require("networkx==2.1")
import networkx as nx


def main():

    filename = 'data/colliders.csv'
    data = np.loadtxt(filename, delimiter=',', dtype='Float64', skiprows=2)
    print("lData:\n{}".format(data))

    polygons, xmin, xmax, ymin, ymax, _, _ = extract_polygons(data)
    zmin = 0
    zmax = 10

    num_samples = 100
    samples = sample_prism( num_samples, xmin, xmax, ymin, ymax, zmin, zmax)

    t0 = time.time()
    filtered_samples = eliminate_collisions(polygons, samples)
    print("eliminate_collisions() took {0} seconds ...".format(time.time() - t0))
    print("filtered_samples.len:{}".format(len(filtered_samples)))

    t0 = time.time()
    graph = create_graph(filtered_samples, 10, polygons)
    print('graph took {0} seconds to build'.format(time.time() - t0))
    print("graph.len:{0}".format(len(graph.edges)))

    # graph stuff
    grid = create_grid(data, zmax, 1)

    nmin = np.min(data[:, 0])
    emin = np.min(data[:, 1])

    plt.rcParams["figure.figsize"] = [6, 6]
    plt.xlabel('EAST')
    plt.ylabel('NORTH')
    plt.imshow(grid, cmap='Greys', origin='lower')

    # draw edges
    for (n1, n2) in graph.edges:
        plt.plot([n1[1] - emin, n2[1] - emin], [n1[0] - nmin, n2[0] - nmin], 'black', alpha=0.5)

    # draw all nodes
    for n1 in filtered_samples:
        plt.scatter(n1[1] - emin, n1[0] - nmin, c='blue')

    # draw connected nodes
    for n1 in graph.nodes:
        plt.scatter(n1[1] - emin, n1[0] - nmin, c='red')

    plt.show()


def can_connect(n1, n2, polygons):
    line = LineString([n1, n2])
    for (p, height) in polygons:
        if p.crosses(line) and p.height >= min(n1[2], n2[2]):
            return False
    return True


def create_graph(nodes, k, polygons):
    g = nx.Graph()
    tree = KDTree(nodes)
    for n1 in nodes:
        # for each node connect try to connect to k nearest nodes
        idxs = tree.query([n1], k, return_distance=False)[0]

        for idx in idxs:
            n2 = nodes[idx]
            # if n2.all() == n1.all():
            #     continue

            if can_connect(n1, n2, polygons):
                g.add_edge(n1, n2, weight=1)
    return g


if __name__ == "__main__":
    main()
