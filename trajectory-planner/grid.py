import numpy as np
import matplotlib.pyplot as plt


def create_grid(data, drone_altitude, safety_distance):

    """
    Returns a grid representation of a 2D configuration space
    based on given obstacle data, drone altitude and safety distance
    arguments.
    """

    # minimum and maximum north coordinates
    north_min = np.floor(np.min(data[:, 0] - data[:, 3]))
    north_max = np.ceil(np.max(data[:, 0] + data[:, 3]))

    # minimum and maximum east coordinates
    east_min = np.floor(np.min(data[:, 1] - data[:, 4]))
    east_max = np.ceil(np.max(data[:, 1] + data[:, 4]))

    # given the minimum and maximum coordinates we can
    # calculate the size of the grid.
    north_size = int(np.ceil((north_max - north_min + 1)))
    east_size = int(np.ceil((east_max - east_min + 1)))

    print("east_min:{}", east_min)
    print("east_max:{}", east_max)
    print("north_size:{}", north_size)
    print("east_size:{}", east_size)

    # Initialize an empty grid
    grid = np.zeros((north_size, east_size))

    print("data.shape:{}", data.shape)
    print("data.shape[0]:{}", data.shape[0])
    print("data.shape[1]:{}", data.shape[1])

    # Populate the grid with obstacles
    for i in range(data.shape[0]):
        north, east, alt, d_north, d_east, d_alt = data[i, :]
        if alt + d_alt + safety_distance > drone_altitude:
            obstacle = [
                int(np.clip(north - d_north - safety_distance - north_min, 0, north_size-1)),
                int(np.clip(north + d_north + safety_distance - north_min, 0, north_size-1)),
                int(np.clip(east - d_east - safety_distance - east_min, 0, east_size-1)),
                int(np.clip(east + d_east + safety_distance - east_min, 0, east_size-1)),
            ]
            grid[obstacle[0]:obstacle[1]+1, obstacle[2]:obstacle[3]+1] = 1

    return grid


def main():

    np.set_printoptions(precision=6)

    plt.rcParams["figure.figsize"] = [6, 6]

    filename = 'data/colliders.csv'
    # Read in the data skipping the first two lines.
    # Note: the first line contains the latitude and longitude of map center
    # Where is this??
    lData = np.loadtxt(filename, delimiter=',', dtype='Float64', skiprows=2)
    print(lData)

    # Static drone altitude (metres)
    lDrone_altitude = 10

    # Minimum distance required to stay away from an obstacle (metres)
    # Think of this as padding around the obstacle.
    lSafe_distance = 3

    lGrid = create_grid(lData, lDrone_altitude, lSafe_distance)

    # equivalent to
    # plt.imshow(np.flip(grid, 0))
    plt.imshow(lGrid, origin='lower')

    plt.xlabel('EAST')
    plt.ylabel('NORTH')

    plt.show()


if __name__ == "__main__":
    main()
