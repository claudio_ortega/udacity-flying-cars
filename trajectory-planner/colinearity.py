import time
import numpy as np


def pointAtHeightOne(p):
    return np.array([p[0], p[1], 1.])


# use numpy for the determinant computation
def collinearity_determinant(p1, p2, p3, epsilon=1e-6):

    pf1 = pointAtHeightOne(p1)
    pf2 = pointAtHeightOne(p2)
    pf3 = pointAtHeightOne(p3)

    print("p1:{}".format(p1))
    print("pf1:{}".format(pf1))

    # Create the matrix out of three points as row, ** notice the double parenthesis
    det = np.linalg.det(np.vstack((pf1, pf2, pf3)))

    print("det:{}".format(det))

    co_linear = False
    if det < epsilon:
        co_linear = True

    return co_linear


# does not use numpy for the determinant computation
def collinearity_alternative(p1, p2, p3):
    co_linear = False

    # will compute in integer arithmetic iff the three are integer
    det = p1[0]*(p2[1] - p3[1]) + p2[0]*(p3[1] - p1[1]) + p3[0]*(p1[1] - p2[1])

    print("det:{}".format(det))

    if det == 0:
        co_linear = True

    return co_linear


# compare numpy ves non-numpy
def compare(p1, p2, p3):

    print("---------------")

    print("time:{}".format(time.time()))

    t1 = time.time()
    collinear_determinant = collinearity_determinant(p1, p2, p3)
    t_determinant = time.time() - t1
    print("collinear_alternative:{}".format(collinear_determinant))
    print("t_3D:{}".format(t_determinant))

    t1 = time.time()
    collinear_alternative = collinearity_alternative(p1, p2, p3)
    t_alternative = time.time() - t1
    print("collinear_alternative:{}".format(collinear_alternative))
    print("t_int:{}".format(t_alternative))

    print("ratio det/alt:{}".format(t_determinant/t_alternative))


"""
this experiment shows by passing to compare() either floats or integers,
how much the ratio in computing times is about 
-- integer/float 
and how much about 
-- the way the results are computed (ie: numpy vs non-numpy)
"""

if __name__ == "__main__":

    # alt method will use integer arithmetic, rate 67
    compare([1, 2], [2, 3], [3, 4])

    # alt method will use integer arithmetic, rate 30
    compare([11, 2], [12, 3], [13, 4])

    # alt method will use also *float* arithmetic, and **rate is 24 !!*
    compare([11.0, 2.0], [12.0, 3.0], [13.0, 4.0])
