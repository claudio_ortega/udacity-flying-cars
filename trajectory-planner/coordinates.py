import utm
import numpy

# converts from global geodetic (longitude, latitude, altitude(up))
# to UTM (easting, northing, zone_number, zone_letter)
# and then to local NED (north, east, down).
def geodetic_to_local(geodetic, geodetic_reference):

    # ref
    longitude_ref = geodetic_reference[0]
    latitude_ref = geodetic_reference[1]
    altitude_ref = geodetic_reference[2]
    (easting_ref, northing_ref, zone_number_ref, zone_letter_ref) = utm.from_latlon(latitude_ref, longitude_ref)

    # non-ref
    longitude = geodetic[0]
    latitude = geodetic[1]
    altitude = geodetic[2]
    (easting, northing, zone_number, zone_letter) = utm.from_latlon(latitude,longitude)

    if zone_number != zone_number_ref:
        raise AssertionError('zone numbers should be the same')

    if zone_letter != zone_letter_ref:
        raise AssertionError('zone letters should be the same')

    return numpy.array([northing-northing_ref, easting-easting_ref, -(altitude-altitude_ref)])

# local_to_geodetic() does the inverse of geodetic_to_local(),
# using the same geodetic_reference
def local_to_geodetic(local_position, geodetic_reference):

    # ref
    longitude_ref = geodetic_reference[0]
    latitude_ref = geodetic_reference[1]
    altitude_ref = geodetic_reference[2]

    (easting_ref, northing_ref, zone_number_ref, zone_letter_ref) = utm.from_latlon(latitude_ref, longitude_ref)

    latAndLong =  utm.to_latlon(
        easting_ref + local_position[1],
        northing_ref + local_position[0],
        zone_number_ref,
        zone_letter_ref)

    return latAndLong[1], latAndLong[0], - ( -altitude_ref + local_position[2] )


if __name__ == "__main__":

    numpy.set_printoptions(precision=6)

    # longitud, latitud, altitud
    geodetic_home = [-122.108432, 37.400154, 20]
    print ( "geodetic_home:{}:".format(geodetic_home))

    local_NED = geodetic_to_local([-122.079465, 37.393037, 30], geodetic_home)

    # Should print [ -764.96  2571.59   -10.  ]
    print ( "local_NED:{}:".format(local_NED))

    # convert back to geodetic
    geodetic_back = local_to_geodetic(local_NED, geodetic_home)

    # Should print [-122.079465, 37.393037, 30 ]
    print ( "geodetic_back:{}:".format(geodetic_back))

    #
    # go the other way now, convert to geodetic first
    geodetic_2 = local_to_geodetic([25.21, 128.07, -30.], geodetic_home)
    print ( "geodetic_2:{}:".format(geodetic_2))

    local_NED_2_back = geodetic_to_local(geodetic_2, geodetic_home)
    print ( "local_NED_2_back:{}:".format(local_NED_2_back))
