import numpy as np
from enum import Enum
from queue import PriorityQueue


class Action(Enum):
    """
    An action is represented by a 3 element tuple.

    The first 2 values are the delta of the action relative
    to the current grid position. The third and final value
    is the cost of performing the action.
    """
    LEFT = (0, -1, 1)
    RIGHT = (0, 1, 1)
    UP = (-1, 0, 1)
    DOWN = (1, 0, 1)

    def __str__(self):
        if self == self.LEFT:
            return '<'
        elif self == self.RIGHT:
            return '>'
        elif self == self.UP:
            return '^'
        elif self == self.DOWN:
            return 'v'

    @property
    def cost(self):
        return self.value[2]

    @property
    def delta(self):
        return self.value[0], self.value[1]


# Define a function that returns a list of valid actions from the current node
def valid_actions(agrid, current_node):

    valid = [Action.UP, Action.LEFT, Action.RIGHT, Action.DOWN]
    n, m = agrid.shape[0] - 1, agrid.shape[1] - 1
    x, y = current_node

    # check if the adjacent node are either off the grid or an obstacle, removing forbidden moves
    if x - 1 < 0 or agrid[x - 1, y] == 1:
        valid.remove(Action.UP)
    if x + 1 > n or agrid[x + 1, y] == 1:
        valid.remove(Action.DOWN)
    if y - 1 < 0 or agrid[x, y - 1] == 1:
        valid.remove(Action.LEFT)
    if y + 1 > m or agrid[x, y + 1] == 1:
        valid.remove(Action.RIGHT)

    return valid


def visualize_path(grid, path, start):
    sgrid = np.zeros(np.shape(grid), dtype=np.str)
    sgrid[:] = ' '
    sgrid[grid[:] == 1] = 'O'

    pos = start

    for a in path:
        da = a.value
        sgrid[pos[0], pos[1]] = str(a)
        pos = (pos[0] + da[0], pos[1] + da[1])
    sgrid[pos[0], pos[1]] = 'G'
    sgrid[start[0], start[1]] = 'S'
    return sgrid


def heuristic_euclidian(position, goal_position):
    h = np.sqrt((position[0] - goal_position[0]) ** 2 + (position[1] - goal_position[1]) ** 2)
    return h


def heuristic_manhattan(position, goal_position):
    h = np.abs(position[0] - goal_position[0]) + np.abs(position[1] - goal_position[1])
    return h


def compute_a_star(grid, h, start, goal):

    queue = PriorityQueue()
    queue.put((0, start))
    visited = set(start)

    branch = {}
    found = False

    while not queue.empty():
        item = queue.get()
        current_cost = item[0]
        current_node = item[1]

        if current_node == goal:
            found = True
            break
        else:
            for action in valid_actions(grid, current_node):
                # get the tuple representation
                da = action.delta
                cost = action.cost
                next_node = (current_node[0] + da[0], current_node[1] + da[1])
                new_cost = current_cost + cost + h(next_node, goal)

                if next_node not in visited:
                    visited.add(next_node)
                    queue.put((new_cost, next_node))

                    branch[next_node] = (new_cost, current_node, action)

    path = []
    path_cost = 0
    if found:

        # retrace steps
        path = []
        n = goal
        path_cost = branch[n][0]
        while branch[n][1] != start:
            path.append(branch[n][2])
            n = branch[n][1]
        path.append(branch[n][2])

    return path[::-1], path_cost, found


def main():

    # Define a start and goal location
    lStart = (0, 0)
    lGoal = (4, 4)

    # Define your grid-based state space of obstacles and free space
    grid = np.array([
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 1, 0, 1],
        [0, 1, 0, 1, 0, 0],
        [0, 0, 0, 1, 1, 0],
        [0, 0, 0, 1, 0, 0],
    ])

    path, cost, found = compute_a_star(grid, heuristic_manhattan, lStart, lGoal)

    print("cost:{}".format(cost))
    print("path.len:{}".format(len(path)))
    print("found:{}".format(found))

    # S -> start, G -> goal, O -> obstacle
    print(visualize_path(grid, path, lStart))


if __name__ == "__main__":
    main()
