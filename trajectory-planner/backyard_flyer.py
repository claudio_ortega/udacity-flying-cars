# author: Claudio Ortega
# submitted on 03/05/2018 9:30PM PST

import argparse
import time
from enum import Enum
import numpy as np
from udacidrone import Drone
from udacidrone.connection import MavlinkConnection, WebSocketConnection  # noqa: F401
from udacidrone.messaging import MsgID

class States(Enum):
    MANUAL = 0
    ARMING = 1
    TAKEOFF = 2
    WAYPOINT = 3
    LANDING = 4
    DISARMING = 5

class BackyardFlyer(Drone):

    def __init__(self, connection):

        super().__init__(connection)

        # register callbacks
        self.register_callback ( MsgID.LOCAL_POSITION, self.position_callback )
        self.register_callback ( MsgID.STATE, self.state_callback )

        # initialize state
        self.target_position = np.array([0, 0, 0])
        self.all_waypoints = []
        self.check_state = {}
        self.flight_state = States.MANUAL


    # registered callbacks -- BEGIN
    def position_callback(self):

        print("position_callback, local_position:{}".format(self.local_position))

        if self.flight_state == States.TAKEOFF:

            total_d_to_target = np.linalg.norm(self.target_position)
            current_d_to_target = np.linalg.norm(self.target_position[0:3] - self.local_position[0:3])

            print("current_d_to_target:{},total_d_to_target:{},".format(
                current_d_to_target,
                total_d_to_target))

            if current_d_to_target < 0.05 * total_d_to_target:

                self.all_waypoints = [
                    [10.0, 0.0, -3.0],
                    [10.0, 10.0, -3.0],
                    [0.0, 10.0, -3.0],
                    [0.0, 0.0, -3.0]
                ]

                self.waypoint_transition()

        elif self.flight_state == States.WAYPOINT:
            # compute distance to way point
            if np.linalg.norm(self.target_position[0:2] - self.local_position[0:2]) < 1.0:
                # is this the last way point?, if so land
                if len(self.all_waypoints) == 0:
                    # but only if we have slowed down enough
                    if (np.linalg.norm(self.local_velocity[0:2]) < 0.5) & \
                       (np.linalg.norm(self.gyro_raw[0:2]) < 0.0001):
                            self.landing_transition()
                else:
                    self.waypoint_transition()

        elif self.flight_state == States.LANDING:
            # disarm, but only if we have slowed down enough
            if ( abs(self.local_position[2]) < 0.1) & (np.linalg.norm(self.local_velocity[0:2]) < 0.1 ):
                self.disarming_transition()


    def state_callback(self):

        print("flight_state:{}".format(self.flight_state))

        if self.flight_state == States.MANUAL:
            if ~self.armed:
                self.arming_transition()

        elif self.flight_state == States.ARMING:
            # we need to wait for this state to be changed by the super class !!
            if self.armed:
                self.takeoff_transition()

        elif self.flight_state == States.DISARMING:
            if ~self.armed:
                self.manual_transition()

    # registered callbacks -- END

    # transition handlers -- BEGIN
    def arming_transition(self):

        print("arming_transition -- BEGIN")

        self.take_control()
        self.arm()

        # so, home is by definition where we are at when arming,
        # all local_posiitons will be relative to this global_position right there
        self.set_home_position(
            self.global_position[0],
            self.global_position[1],
            self.global_position[2])

        self.cmd_position(0.0, 0.0, 0.0, 0.0)

        print("arming_transition, home position:{}".format(self.global_position))
        print("arming_transition, local_position:{}".format(self.local_position))

        self.flight_state = States.ARMING

        print("arming_transition -- END")

    def takeoff_transition(self):
        print("takeoff_transition -- BEGIN")

        # this is how much t he problem definition asks us to climb
        self.target_position[0:3] = [0.0, 0.0, -3.0]
        self.takeoff(-self.target_position[2])
        self.flight_state = States.TAKEOFF

        print("takeoff_transition -- END")

    def waypoint_transition(self):
        print("waypoint_transition -- BEGIN")

        self.target_position = self.all_waypoints.pop(0)
        self.cmd_position(self.target_position[0], self.target_position[1], self.target_position[2], 0.0)
        self.flight_state = States.WAYPOINT

        print("waypoint_transition -- END")

    def landing_transition(self):
        print("landing_transition -- BEGIN")

        # keeps the x,y coordinates untouched, only changes z -> 0
        self.land()
        self.flight_state = States.LANDING

        print("landing_transition -- END")

    def disarming_transition(self):
        print("disarming_transition -- BEGIN")

        self.disarm()

        # this is to tell mavlink that we are not controlling anymore
        self.release_control()
        self.flight_state = States.DISARMING

        print("disarming_transition -- END")

    def manual_transition(self):
        print("manual_transition -- BEGIN")

        self.stop()
        self.flight_state = States.MANUAL
        self.release_control()

        print("manual_transition -- END")

    def start(self):
        self.start_log("Logs", "NavLog.txt")
        super().start()

    # transition handlers -- END


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default='127.0.0.1', help="host address, i.e. '127.0.0.1'")
    parser.add_argument('--port', type=int, default=5760, help='port number')

    args = parser.parse_args()
    conn = MavlinkConnection('tcp:{0}:{1}'.format(args.host, args.port), threaded=False, PX4=False)
    drone = BackyardFlyer(conn)

    print("waiting before initializing")
    time.sleep(2)
    print("done waiting")

    drone.start()
